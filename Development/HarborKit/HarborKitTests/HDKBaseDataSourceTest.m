//
//  HDKBaseDataSourceTest.m
//  HarborKit
//
//  Created by Joseph Gorecki on 10/21/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HDKBaseDataSource.h"
#import "HDKDataSourceable.h"
#import "HDKDataSourceConstants.h"

@interface HDKBaseDataSourceTest : XCTestCase <HDKDataSourceable>

@property (nonatomic, strong) HDKBaseDataSource *dataSource;
@property (nonatomic, copy) NSArray *testArray;
@property (readwrite) BOOL delegateCallbackHappened;

@end

@implementation HDKBaseDataSourceTest

- (void)setUp {
    
    [super setUp];
    
    UITableView *aTableView = [[UITableView alloc] init];
    
    HDKCellBlock cellBlock = ^(UITableViewCell *cell, id object){
    
        UITableViewCell *aCell = cell;
        aCell.textLabel.text = object;
        
    };
    
    self.testArray = @[@"One", @"Two", @"Three"];
    
    self.dataSource = [[HDKBaseDataSource alloc] init];
    self.dataSource.arr = self.testArray;
    self.dataSource.tableView = aTableView;
    self.dataSource.cellBlock = cellBlock;
    self.dataSource.relatedObject = [NSString string];
    self.dataSource.offset = 5;
    self.dataSource.limit = 5;
    
    self.dataSource.delegate = self;
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    
    self.dataSource = nil;
    self.dataSource.delegate = nil;
    self.testArray = nil;
}

//@property (copy, nonatomic) NSArray *arr;
- (void)testDataSourceArray{
    
    self.dataSource.arr = self.testArray;
    
    XCTAssertEqual(self.testArray, self.dataSource.arr);
    XCTAssertNotNil(self.dataSource.arr);

}

- (void)testDataSourceCellBlock{

    XCTAssertNotNil(self.dataSource.cellBlock);

}

- (void)testDataSourceRelatedItem{
    
    XCTAssertNotNil(self.dataSource.relatedObject);
    
}

- (void)testDataSourceOffset{
    
    XCTAssert(self.dataSource.offset > 0);
    
}

- (void)DISABLED_testDataSourceEditable{
    
    XCTAssert(self.dataSource.editable == NO);
    XCTAssert(self.dataSource.tableView.editing == NO);
    
    self.dataSource.editable = YES;
    
    XCTAssert(self.dataSource.editable == YES);
    XCTAssert(self.dataSource.tableView.editing == YES);
    
}

- (void)DISABLED_testDelegate{

    [self.dataSource.tableView.delegate tableView:self.dataSource.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];

    XCTAssertTrue(_delegateCallbackHappened);
    
}

- (void)DISABLED_didSelectRowWithObject:(id)anObject andCompletion:(HDKCompletionBlock)completion{
    
    _delegateCallbackHappened = YES;

}

//
//@property (strong, nonatomic) UITableView *tableView;

//- (instancetype)initWithTableView:(UITableView *)aTableView
//                        cellBlock:(HDKCellBlock)aCellBlock
//                            array:(NSArray *)anArray
//                         delegate:(id<HDKDataSourceable>)aDelegate;
//
//- (void)reloadData:(HDKCompletionBlock)completion;
//- (void)reloadDataWithSelections:(NSSet *)selections andCompletion:(HDKCompletionBlock)completion;
//
//- (void)tableCellInternalButtonClick:(id)sender;


@end
