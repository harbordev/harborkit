//
//  HDKValidationHelperTest.m
//  HarborKit
//
//  Created by Joseph Gorecki on 10/20/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HDKValidationHelper.h"

@interface HDKValidationHelperTest : XCTestCase

@end

@implementation HDKValidationHelperTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testIsValidEntry{

    BOOL sut;
    
    sut = [HDKValidationHelper isValidEntry:@"a non-blank field"];

    XCTAssertTrue(sut, @"should pass when text is provided");
    
    sut = [HDKValidationHelper isValidEntry:@""];
    
    XCTAssertFalse(sut, @"should reject when text is ommited");
    
    sut = [HDKValidationHelper isValidEntry:nil];
    
    XCTAssertFalse(sut, @"should rejext when text is missing");
    
}

- (void)testIsValidEmail{

    BOOL sut;
    
    sut = [HDKValidationHelper isValidEmailAddress:@"j@harbordev.com"];
    
    XCTAssertTrue(sut, @"should pass when email is provided");
    
    sut = [HDKValidationHelper isValidEmailAddress:@"blargh"];
    
    XCTAssertFalse(sut, @"should reject when email is wrong format");
    
    sut = [HDKValidationHelper isValidEmailAddress:nil];
    
    XCTAssertFalse(sut, @"should rejext when email is missing");
    
    sut = [HDKValidationHelper isValidEmailAddress:@".com@j"];

    XCTAssertFalse(sut, @"should rejext when email is formatted poorly");

}

- (void)testIsValidNumberString{
    
    BOOL sut;
    
    sut = [HDKValidationHelper isValidNumberString:@"123456"];
    
    XCTAssertTrue(sut, @"should pass when numbers are provided");
    
    sut = [HDKValidationHelper isValidNumberString:@""];
    
    XCTAssertFalse(sut, @"should reject when numbers are ommited");
    
    sut = [HDKValidationHelper isValidNumberString:nil];
    
    XCTAssertFalse(sut, @"should rejext when numbers are missing");
    
    sut = [HDKValidationHelper isValidNumberString:@"onetwothree"];
    
    XCTAssertFalse(sut, @"should rejext when numbers are wrong");
    
}

- (void)testIsValidPasswordSimple{
    
    BOOL sut;
    
    sut = [HDKValidationHelper isValidPasswordSimple:@"pwd1"];
    
    XCTAssertTrue(sut, @"should pass when a 4 character password is provided");

    sut = [HDKValidationHelper isValidPasswordSimple:@"pwd123456789101112131415"];
    
    XCTAssertFalse(sut, @"should reject when a -4 character password is provided");
    
    sut = [HDKValidationHelper isValidPasswordSimple:@"pwd123456789101112131415"];
    
    XCTAssertFalse(sut, @"should reject when a +15 character password is provided");
    
    sut = [HDKValidationHelper isValidPasswordSimple:@""];
    
    XCTAssertFalse(sut, @"should reject when password are ommited");
    
    sut = [HDKValidationHelper isValidPasswordSimple:nil];
    
    XCTAssertFalse(sut, @"should rejext when password are missing");
    
}

// *Password is not blank and must be between 4-15 characters, and include a number
- (void)testIsValidPasswordMedium{
    
    BOOL sut;
    
    sut = [HDKValidationHelper isValidPasswordMedium:@"pwd1"];
    
    XCTAssertTrue(sut, @"should pass when a formatted 4 character password is provided");
    
    sut = [HDKValidationHelper isValidPasswordMedium:@"pwd123456789101112131415"];
    
    XCTAssertFalse(sut, @"should reject when a -4 character password is provided");
    
    sut = [HDKValidationHelper isValidPasswordMedium:@"pwd123456789101112131415"];
    
    XCTAssertFalse(sut, @"should reject when a +15 character password is provided");
    
    sut = [HDKValidationHelper isValidPasswordMedium:@""];
    
    XCTAssertFalse(sut, @"should reject when password are ommited");
    
    sut = [HDKValidationHelper isValidPasswordMedium:nil];
    
    XCTAssertFalse(sut, @"should rejext when password are missing");
    
    sut = [HDKValidationHelper isValidPasswordMedium:@"pwdinwrongformat"];
    
    XCTAssertFalse(sut, @"should rejext when password doesn't include a number");
    
}

// *Password is not blank, must be between 4-15 characters, must include a number, a symbol, and uppercase
- (void)testIsValidPasswordComplex{
    
    BOOL sut;
    
    sut = [HDKValidationHelper isValidPasswordComplex:@"Pwd1!"];
    
    XCTAssertTrue(sut, @"should pass when a formatted 4 character password is provided");
    
    sut = [HDKValidationHelper isValidPasswordComplex:@"Pwd123456789101112131415!"];
    
    XCTAssertFalse(sut, @"should reject when a -4 character password is provided");
    
    sut = [HDKValidationHelper isValidPasswordComplex:@"Pwd123456789101112131415!"];
    
    XCTAssertFalse(sut, @"should reject when a +15 character password is provided");
    
    sut = [HDKValidationHelper isValidPasswordComplex:@""];
    
    XCTAssertFalse(sut, @"should reject when password are ommited");
    
    sut = [HDKValidationHelper isValidPasswordComplex:nil];
    
    XCTAssertFalse(sut, @"should rejext when password are missing");
    
    sut = [HDKValidationHelper isValidPasswordComplex:@"Pwdinwrongformat!"];
    
    XCTAssertFalse(sut, @"should rejext when password doesn't include a number");
    
    sut = [HDKValidationHelper isValidPasswordComplex:@"pwdinwrongformat1!"];
    
    XCTAssertFalse(sut, @"should rejext when password doesn't include an uppercase character");
    
//    sut = [HDKValidationHelper isValidPasswordComplex:@"Pwdinwrongformat1!"];
//    
//    XCTAssertFalse(sut, @"should reject when password doesn't include an special character");
    
}

@end
