//
//  HDKHttpResponseHelperTest.m
//  HarborKit
//
//  Created by Joseph Gorecki on 10/20/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HDKHttpResponseHelper.h"

@interface HDKHttpResponseHelperTest : XCTestCase

@end

@implementation HDKHttpResponseHelperTest

- (void)setUp {

    [super setUp];
}

- (void)tearDown{

    [super tearDown];

}

- (void)testIsInValidRange{
    
    BOOL sut;
    
    NSHTTPURLResponse *passing_response_low = [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@""] statusCode:200 HTTPVersion:nil headerFields:@{}];
    
    sut = [HDKHttpResponseHelper isInValidRange:passing_response_low];
    
    XCTAssertTrue(sut, @"200 response goes through correctely");
    
    NSHTTPURLResponse *passing_response_high = [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@""] statusCode:299 HTTPVersion:nil headerFields:@{}];
    
    sut = [HDKHttpResponseHelper isInValidRange:passing_response_high];
    
    XCTAssertTrue(sut, @"299 response goes through correctely");
    
}

- (void)testIsOutOfValidRange{

    BOOL sut;
    
    NSHTTPURLResponse *failing_response_low = [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@""] statusCode:199 HTTPVersion:nil headerFields:@{}];
    
    sut = [HDKHttpResponseHelper isInValidRange:failing_response_low];
    
    XCTAssertFalse(sut, @"bad response fails correctly");
    
    NSHTTPURLResponse *failing_response_high = [[NSHTTPURLResponse alloc] initWithURL:[NSURL URLWithString:@""] statusCode:300 HTTPVersion:nil headerFields:@{}];
    
    sut = [HDKHttpResponseHelper isInValidRange:failing_response_high];
    
    XCTAssertFalse(sut, @"bad response fails correctly");

}

@end
