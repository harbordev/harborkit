//
//  HDKDateManagerTest.m
//  HarborKit
//
//  Created by Joseph Gorecki on 10/20/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HDKDateManager.h"

@interface HDKDateManagerTest : XCTestCase

@end

@implementation HDKDateManagerTest

- (void)setUp {
    
    [super setUp];

}

- (void)tearDown {

    [super tearDown];
}

#pragma mark - Singleton Helpers
- (HDKDateManager *)createUniqueInstance{

    return [[HDKDateManager alloc] init];

}

- (HDKDateManager *)getSharedInstance{

    return [HDKDateManager sharedInstance];

}

#pragma mark - Singleton tests
- (void)testSingletonSharedInstanceCreated {
    
    XCTAssertNotNil([self getSharedInstance]);
    
}

- (void)testSingletonUniqueInstanceCreated {
    
    XCTAssertNotNil([self createUniqueInstance]);
    
}

- (void)testSingletonReturnsSameSharedInstanceTwice {
    
    id sut = [self getSharedInstance];
    XCTAssertEqual(sut, [self getSharedInstance]);
    
}

- (void)testSingletonSharedInstanceSeparateFromUniqueInstance {
    
    id sut = [self getSharedInstance];
    XCTAssertNotEqual(sut, [self createUniqueInstance]);
    
}

- (void)testSingletonReturnsSeparateUniqueInstances {
    
    id sut = [self createUniqueInstance];
    XCTAssertNotEqual(sut, [self createUniqueInstance]);
    
}

#pragma mark - DateFormatter Tests
- (void)testDateFormatter{

    //@property (strong, nonatomic) NSDateFormatter *formatter;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    HDKDateManager *sut = [self getSharedInstance];
    
    sut.formatter = dateFormatter;
    
    XCTAssertEqual(dateFormatter, sut.formatter);
    
    XCTAssertEqualObjects(dateFormatter, sut.formatter);

}

//(NSDate *)formatFromStringWithT:(NSString *)aString;
- (void)testFormatFromStringWithT{

    HDKDateManager *sut = [self getSharedInstance];
    
    NSString *passing_dateSringForTesting = @"2016-10-11T15:47:32.000-07:00"; //becomes 2016-10-11 15:47:32 +0000
    
    NSDate *apassing_Date = [sut formatFromStringWithT:passing_dateSringForTesting];
    
    XCTAssertNotNil(apassing_Date);

    NSString *failing_dateSringForTesting = @"2016-10-11 15:47:32.000-07:00"; //becomes nil
    
    NSDate *failing_Date = [sut formatFromStringWithT:failing_dateSringForTesting];
    
    XCTAssertNil(failing_Date);
    
}

//- (NSDate *)formatFromString:(NSString *)aString;
- (void)testFormatFromString{
    
    HDKDateManager *sut = [self getSharedInstance];
    
    NSString *passing_dateSringForTesting = @"2016-10-11 15:47:32"; //becomes 2016-10-11 15:47:32 +0000
    
    NSDate *apassing_Date = [sut formatFromString:passing_dateSringForTesting];
    
    XCTAssertNotNil(apassing_Date);
    
    NSString *failing_dateSringForTesting = @"2016-10-11 15:47:32.000-07:00"; //becomes nil
    
    NSDate *failing_Date = [sut formatFromString:failing_dateSringForTesting];
    
    XCTAssertNil(failing_Date);
    
}

//- (NSString *)formatedDateStringFromString:(NSString *)aString;
//- (NSString *)formatedDateStringFromTString:(NSString *)aString;

@end
