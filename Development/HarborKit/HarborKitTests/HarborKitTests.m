//
//  HarborKitTests.m
//  HarborKitTests
//
//  Created by Joseph Gorecki on 7/13/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

@interface HarborKitTests : XCTestCase

@end

@implementation HarborKitTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

//- (void)testAsynchronousURLConnection {
//    NSURL *URL = [NSURL URLWithString:@"http://nshipster.com/"];
//    NSString *description = [NSString stringWithFormat:@"GET %@", URL];
//    XCTestExpectation *expectation = [self expectationWithDescription:description];
//    
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *task = [session dataTaskWithURL:URL
//                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
//                                  {
//                                      XCTAssertNotNil(data, "data should not be nil");
//                                      XCTAssertNil(error, "error should be nil");
//                                      
//                                      if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
//                                          NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//                                          XCTAssertEqual(httpResponse.statusCode, 200, @"HTTP response status code should be 200");
//                                          XCTAssertEqualObjects(httpResponse.URL.absoluteString, URL.absoluteString, @"HTTP response URL should be equal to original URL");
//                                          XCTAssertEqualObjects(httpResponse.MIMEType, @"text/html", @"HTTP response content type should be text/html");
//                                      } else {
//                                          XCTFail(@"Response was not NSHTTPURLResponse");
//                                      }
//                                      
//                                      [expectation fulfill];
//                                  }];
//    
//    [task resume];
//    
//    [self waitForExpectationsWithTimeout:task.originalRequest.timeoutInterval handler:^(NSError *error) {
//        if (error != nil) {
//            NSLog(@"Error: %@", error.localizedDescription);    
//        }
//        [task cancel];
//    }];
//}

@end
