//
//  ViewController.m
//  HarborKit
//
//  Created by Joseph Gorecki on 7/13/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "ViewController.h"
#import "HDKHudHelper.h"
#import "HDKFacebookManager.h"
#import "HDKImageSelectorViewController.h"

#import "PresentedViewController.h"

#import <MobileCoreServices/MobileCoreServices.h>

@interface ViewController ()<HDKImageSelectorViewControllerDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

- (IBAction)shareLinkWithFacebook:(id)sender {

    [[HDKFacebookManager sharedInstance] shareLinkToFacebook:[NSURL URLWithString:@"http://www.google.com"] title:@"Google" description:@"Search engine" fromViewController:self completion:nil];

}

- (IBAction)shareImageWithFacebook:(id)sender {
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[PresentedViewController new]];
    
    [self presentViewController:navigationController animated:YES completion:nil];
    
}

#pragma mark - HDKImageSelectorViewControllerDelegate
- (void)didFinishPickingImage:(UIImage *)anImage withMovieURL:(NSString *)aURLString{

    

}

@end
