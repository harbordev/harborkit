//
//  PresentedViewController.m
//  HarborKit
//
//  Created by Joseph Gorecki on 8/24/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import "PresentedViewController.h"
#import "HDKImageSelectorViewController.h"

@interface PresentedViewController ()<HDKImageSelectorViewControllerDelegate>

@end

@implementation PresentedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)shareImage:(id)sender {
    
    HDKImageSelectorViewController *vc = [HDKImageSelectorViewController createImageSelectionControllerWithViewController:self andDelegate:self];
    
    [vc showActionSheet];
}



- (IBAction)pushAnotherController:(id)sender{

    [self.navigationController pushViewController:[PresentedViewController new] animated:YES];

}

- (void)didFinishPickingImage:(UIImage *)anImage withMovieURL:(NSString *)aURLString{

    //NSLog(@"completed: %@ - %@", anImage, aURLString);
    

}

@end
