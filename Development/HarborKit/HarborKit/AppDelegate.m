//
//  AppDelegate.m
//  HarborKit
//
//  Created by Joseph Gorecki on 7/13/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "AppDelegate.h"

#import "HDKLoginProcess.h"
#import "ViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    ViewController *rootViewController = [[ViewController alloc] init];
    
    HDKLoginProcess *loginProcess = [HDKLoginProcess new];
    
    loginProcess.loginPath = @"https://favement.herokuapp.com/api/v1/user/login/";
    loginProcess.signupPath = @"https://favement.herokuapp.com/api/v1/user/create/";
    
    loginProcess.loginNib = @"HDKLoginProcessLoginViewController";
    loginProcess.signupNib = @"HDKLoginProcessSignUpViewController";
    loginProcess.welcomeNib = @"HDKLoginProcessWelcomeViewController";
    
    [loginProcess startLoginProcessWithApplication:rootViewController];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationShouldShowRootController:) name:HDKUserDefaultsWasChanged object:nil];
    
    //DDLogWarn(@" --- loading application up: %@", [[NSBundle mainBundle] bundlePath]);
    
    self.window.rootViewController = rootViewController;
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
 
}

#pragma mark - FBCSupport
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}


@end
