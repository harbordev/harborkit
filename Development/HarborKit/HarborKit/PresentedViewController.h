//
//  PresentedViewController.h
//  HarborKit
//
//  Created by Joseph Gorecki on 8/24/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import "HDKBaseViewController.h"

@interface PresentedViewController : HDKBaseViewController

@end
