//
//  NSString+HarborKit.m
//  HarborKit
//
//  Created by Joseph Gorecki on 9/3/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "NSString+HarborKit.h"
#import <DateTools/DateTools.h>

@implementation NSString (HarborKit)

- (NSString *)capitalizeFirstLetter{
    
    //capitalizes first letter of a NSString
    //find position of first alphanumeric charecter (compensates for if the string starts with space or other special character)
    if (self.length<1) {
        return @"";
    }
    NSRange firstLetterRange = [self rangeOfCharacterFromSet:[NSCharacterSet alphanumericCharacterSet]];
    if (firstLetterRange.location > self.length)
        return self;
    
    return [self stringByReplacingCharactersInRange:NSMakeRange(firstLetterRange.location,1) withString:[[self substringWithRange:NSMakeRange(firstLetterRange.location, 1)] capitalizedString]];
    
}

- (NSString *)capitalizeSentences{
    
    NSString *inputString = [self copy];
    
    //capitalize the first letter of the string
    NSString *outputStr = [inputString capitalizeFirstLetter];
    
    //capitalize every first letter after "."
    NSArray *sentences = [outputStr componentsSeparatedByString:@"."];
    outputStr = @"";
    for (NSString *sentence in sentences){
        static int i = 0;
        if (i<sentences.count-1)
            outputStr = [outputStr stringByAppendingString:[NSString stringWithFormat:@"%@.",[sentence capitalizeFirstLetter]]];
        else
            outputStr = [outputStr stringByAppendingString:[sentence capitalizeFirstLetter]];
        i++;
    }
    
    //capitalize every first letter after "?"
    sentences = [outputStr componentsSeparatedByString:@"?"];
    outputStr = @"";
    for (NSString *sentence in sentences){
        static int i = 0;
        if (i<sentences.count-1)
            outputStr = [outputStr stringByAppendingString:[NSString stringWithFormat:@"%@?",[sentence capitalizeFirstLetter]]];
        else
            outputStr = [outputStr stringByAppendingString:[sentence capitalizeFirstLetter]];
        i++;
    }
    //capitalize every first letter after "!"
    sentences = [outputStr componentsSeparatedByString:@"!"];
    outputStr = @"";
    for (NSString *sentence in sentences){
        static int i = 0;
        if (i<sentences.count-1)
            outputStr = [outputStr stringByAppendingString:[NSString stringWithFormat:@"%@!",[sentence capitalizeFirstLetter]]];
        else
            outputStr = [outputStr stringByAppendingString:[sentence capitalizeFirstLetter]];
        i++;
    }
    
    return outputStr;
}

- (NSString *)formattedTimeAgoFromTString:(NSString *)aString{

    NSArray *anArray = [aString componentsSeparatedByString:@"."];//remove any microseconds
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
    
    NSDate *aDate = [formatter dateFromString:[anArray firstObject]];
    
    return [aDate timeAgoSinceNow];

}

@end
