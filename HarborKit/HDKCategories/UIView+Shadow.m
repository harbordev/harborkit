//
//  UIView+Shadow.m
//  HangOn
//
//  Created by Joseph Gorecki on 3/13/14.
//  Copyright (c) 2014 Joseph Gorecki. All rights reserved.
//

#import "UIView+Shadow.h"

@implementation UIView (Shadow)

- (void)addBackgroundShadow:(UIColor *)shadowColor CGSize:(CGSize)CGSize shadowOpacity:(float)shadowOpacity shadowRadius:(float)shadowRadius
{
    self.layer.shadowColor = shadowColor.CGColor;
    self.layer.shadowOffset = CGSize;
    self.layer.shadowOpacity = shadowOpacity;
    self.layer.shadowRadius = shadowRadius;
    self.clipsToBounds = NO;
}

@end
