//
//  UITextField+PlaceholderColor.m
//  PogoMe
//
//  Created by Joseph Gorecki on 7/23/14.
//  Copyright (c) 2014 HarborDev. All rights reserved.
//

#import "UITextField+PlaceholderColor.h"

@interface UITextField (OPConvenience)

@property (strong, nonatomic) UIColor* placeholderColor;

@end

@implementation UITextField (PlaceholderColor)

- (void)setPlaceholderColor:(UIColor *)color{
    
    if (color) {
        
        NSMutableAttributedString* attrString = [self.attributedPlaceholder mutableCopy];
        
        [attrString setAttributes: @{NSForegroundColorAttributeName: color} range: NSMakeRange(0,  attrString.length)];
    
        self.attributedPlaceholder = attrString;
    }
    
}

- (UIColor *)placeholderColor{
    
    return [self.attributedPlaceholder attribute:NSForegroundColorAttributeName atIndex:0 effectiveRange:NULL];
}

@end
