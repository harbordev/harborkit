//
//  UIView+Shadow.h
//  HangOn
//
//  Created by Joseph Gorecki on 3/13/14.
//  Copyright (c) 2014 Joseph Gorecki. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Category on a UIView to add a shadow to it.
 */

@interface UIView (Shadow)

- (void)addBackgroundShadow:(UIColor *)shadowColor CGSize:(CGSize)CGSize shadowOpacity:(float)shadowOpacity shadowRadius:(float)shadowRadius;

@end
