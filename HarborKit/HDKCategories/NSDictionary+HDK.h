//
//  NSDictionary+FMT.h
//  Favement
//
//  Created by Joseph Gorecki on 4/30/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Category on NSDictonary to turn a NSDictionary into a json string representation
 */
@interface NSDictionary (HDK)

- (NSString *)hdk_jsonString;

@end
