//
//  UITextField+PlaceholderColor.h
//  PogoMe
//
//  Created by Joseph Gorecki on 7/23/14.
//  Copyright (c) 2014 HarborDev. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Category on UITextField to change the color of its placeholder
 */
@interface UITextField (PlaceholderColor)

- (void)setPlaceholderColor:(UIColor *)color;
- (UIColor *)placeholderColor;

@end
