//
//  UIImage+FixOrientation.h
//  HangOn
//
//  Created by Joseph Gorecki on 3/10/14.
//  Copyright (c) 2014 Joseph Gorecki. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Category on UIImage to fix orientation
 */

@interface UIImage (FixOrientation)

- (UIImage *)fixOrientation;

@end
