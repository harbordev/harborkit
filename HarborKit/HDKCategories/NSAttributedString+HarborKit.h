//
//  NSAttributedString+HarborKit.h
//  HarborKit
//
//  Created by Joseph Gorecki on 9/3/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

/**
 *  Catecory on NSAttributedString to add ellipses when a string is to long
 */

@interface NSAttributedString (HarborKit)

- (NSAttributedString *)addEllipses:(NSUInteger)aMax withHighLightedColor:(UIColor *)aColor andURLString:(NSString *)aUrlString;

- (NSAttributedString *)addEllipsesWithoutText:(NSUInteger)aMax withHighLightedColor:(UIColor *)aColor;

@end
