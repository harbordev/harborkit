//
//  NSDictionary+FMT.m
//  Favement
//
//  Created by Joseph Gorecki on 4/30/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "NSDictionary+HDK.h"

@implementation NSDictionary (HDK)

- (NSString *)hdk_jsonString{
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:0 error:&error];
    
    if (!jsonData) {
        
        //NSLog(@"%s : %@", __PRETTY_FUNCTION__, error.localizedDescription);
        
        return nil;

    } else {
    
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }

}

@end
