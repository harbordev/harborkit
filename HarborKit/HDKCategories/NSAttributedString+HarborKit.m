//
//  NSAttributedString+HarborKit.m
//  HarborKit
//
//  Created by Joseph Gorecki on 9/3/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "NSAttributedString+HarborKit.h"
#import <MWFeedParser/NSString+HTML.h>

@implementation NSAttributedString (HarborKit)

- (NSAttributedString *)addEllipses:(NSUInteger)aMax withHighLightedColor:(UIColor *)aColor andURLString:(NSString *)aUrlString{
    
    NSAttributedString *aString = self;
    
    if (self.length > aMax) {
        
        NSAttributedString *readMore = [[NSAttributedString alloc] initWithString:@"...  read more" attributes:@{NSForegroundColorAttributeName:aColor}];
        
        NSString *segmented = [[self.string stringByConvertingHTMLToPlainText] substringWithRange:NSMakeRange(0, aMax - [readMore string].length)];
        
        NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:segmented];
        
        [attributed appendAttributedString:readMore];
        
        return [attributed copy];
        
    }
    
    return aString;
    
}

- (NSAttributedString *)addEllipsesWithoutText:(NSUInteger)aMax withHighLightedColor:(UIColor *)aColor;{
    
    NSAttributedString *aString = self;
    
    if (self.length > aMax) {
        
        NSAttributedString *readMore = [[NSAttributedString alloc] initWithString:@"..." attributes:@{NSForegroundColorAttributeName:aColor}];
        
        NSString *segmented = [[self.string stringByConvertingHTMLToPlainText] substringWithRange:NSMakeRange(0, aMax - [readMore string].length)];
        
        NSMutableAttributedString *attributed = [[NSMutableAttributedString alloc] initWithString:segmented];
        
        [attributed appendAttributedString:readMore];
        
        return [attributed copy];
        
    }
    
    return aString;
    
}

@end
