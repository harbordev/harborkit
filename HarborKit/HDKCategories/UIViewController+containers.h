//
//  UIViewController+containers.h
//  Sabian
//
//  Created by Joe on 6/10/13.
//  Copyright (c) 2013 Joe. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Category on UIViewController for adding and removing subcontrollers 
 */
@interface UIViewController (containers)

- (void)containerAddChildViewController:(UIViewController *)childViewController parentView:(UIView *)view;

- (void)containerAddChildViewController:(UIViewController *)childViewController;

- (void)containerRemoveChildViewController:(UIViewController *)childViewController;

@end
