//
//  NSString+HarborKit.h
//  HarborKit
//
//  Created by Joseph Gorecki on 9/3/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Category on NSString to cap words and sentences
 */

@interface NSString (HarborKit)

-(NSString *)capitalizeFirstLetter;
-(NSString *)capitalizeSentences;

-(NSString *)formattedTimeAgoFromTString:(NSString *)aString;

@end
