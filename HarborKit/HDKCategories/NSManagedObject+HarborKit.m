//
//  NSManagedObject+HarborKit.m
//  HarborKit
//
//  Created by Joseph Gorecki on 8/21/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "NSManagedObject+HarborKit.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation NSManagedObject (HarborKit)

+ (instancetype)findOrCreateClass:(Class)aClass forServer:(NSNumber *)aServerID inContext:(NSManagedObjectContext *)aContext{
    
    NSParameterAssert([aServerID isKindOfClass:[NSNumber class]]);
    
    id anObject = [[aClass class] MR_findFirstByAttribute:@"server_id" withValue:aServerID inContext:aContext];
    
    if(anObject == nil){
        
        anObject = [[aClass class] MR_createEntityInContext:aContext];
        
        NSError *anError = nil;
        
        [aContext save:&anError];
        
        if(anError){
            
            //NSLog(@"A terrible error occurred: %s", __PRETTY_FUNCTION__);
            
        }
        
    }
    
    return anObject;
    
}

- (void)safeSetValuesForKeysWithDictionary:(NSDictionary *)keyedValues{
    
    NSDictionary *attributes = [[self entity] attributesByName];
    
    for (NSString *attribute in attributes) {
        
        ////NSLog(@"attribute: %@", attribute);
        
        id value = [keyedValues objectForKey:attribute];
        
        ////NSLog(@"attempting to set: %@ %@", value, [value class]);
        
        if ((value == nil) || ([value isKindOfClass:[NSNull class]])) {
            // Don't attempt to set nil, or you'll overwite values in self that aren't present in keyedValues
            ////NSLog(@"--skipping %@", value);
            continue;
        }
        
        NSAttributeType attributeType = [[attributes objectForKey:attribute] attributeType];
        
        if ((attributeType == NSStringAttributeType) && ([value isKindOfClass:[NSNumber class]])) {
            value = [value stringValue];
            
        } else if (((attributeType == NSInteger16AttributeType) || (attributeType == NSInteger32AttributeType) || (attributeType == NSInteger64AttributeType) || (attributeType == NSBooleanAttributeType)) && ([value isKindOfClass:[NSString class]])) {
            value = [NSNumber numberWithInteger:[value  integerValue]];
            
        } else if ((attributeType == NSFloatAttributeType) && ([value isKindOfClass:[NSString class]])) {
            
            value = [NSNumber numberWithDouble:[value doubleValue]];
        }
        
        [self setValue:value forKey:attribute];
        
        ////NSLog(@"=======================");
        
    }
}

#pragma mark - Placeholder categories.
+ (void)parseJSONIntoDataStore:(id)json withRelated:(id)obj andCompletion:(HDKCompletionBlock)completion{

    assert(@"subclass in model only.");

}

+ (void)createdObjectResponse:(NSDictionary *)aServerResponse andRelatedId:(id)anObject andCompletion:(HDKCreatedCompletionBlock)completio{

    assert(@"subclass in model only.");

}

+ (void)getObjectFromServerWithDict:(NSDictionary *)aDict withRelated:(id)obj andCompletion:(HDKCompletionBlock)completion{
    
    assert(@"not ready yet.");
    
}

@end
