//
//  CLPlacemark+StateAbbreviation.h
//  HangOn
//
//  Created by Joseph Gorecki on 4/1/14.
//  Copyright (c) 2014 Joseph Gorecki. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

/**
 *  Dumb Category on CLPlacemark to get turn a State Abbreviation into the actual State name
 */

@interface CLPlacemark (StateAbbreviation)

- (NSString *)shortState;

@end
