//
//  UIViewController+containers.m
//  Sabian
//
//  Created by Joe on 6/10/13.
//  Copyright (c) 2013 Joe. All rights reserved.
//

#import "UIViewController+containers.h"

@implementation UIViewController (containers)

- (void)containerAddChildViewController:(UIViewController *)childViewController parentView:(UIView *)view {
    
    [self addChildViewController:childViewController];
    [view addSubview:childViewController.view];
    [childViewController didMoveToParentViewController:self];
    
}

- (void)containerAddChildViewController:(UIViewController *)childViewController {
    
    [self containerAddChildViewController:childViewController parentView:self.view];
    
}

- (void)containerRemoveChildViewController:(UIViewController *)childViewController {
    
    [childViewController willMoveToParentViewController:nil];
    [childViewController.view removeFromSuperview];
    [childViewController removeFromParentViewController];
    
}

@end