//
//  NSManagedObject+HarborKit.h
//  HarborKit
//
//  Created by Joseph Gorecki on 8/21/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "HDKDataSourceConstants.h"

/**
 *  Category on NSManagedObject to add
 */

@interface NSManagedObject (HarborKit)

+ (instancetype)findOrCreateClass:(Class)aClass forServer:(NSNumber *)aServerID inContext:(NSManagedObjectContext *)aContext;

- (void)safeSetValuesForKeysWithDictionary:(NSDictionary *)keyedValues;

/**
 * Placeholders for Sync Service -- should be in override in project models.  These are only here so HarborKit could compile with the methods.
 */
+ (void)parseJSONIntoDataStore:(id)json withRelated:(id)obj andCompletion:(HDKCompletionBlock)completion;
+ (void)createdObjectResponse:(NSDictionary *)aServerResponse andRelatedId:(id)anObject andCompletion:(HDKCreatedCompletionBlock)completion;
+ (void)getObjectFromServerWithDict:(NSDictionary *)aDict withRelated:(id)obj andCompletion:(HDKCompletionBlock)completion;

@end
