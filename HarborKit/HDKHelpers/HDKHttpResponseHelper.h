//
//  HDKHttpResponseHelper.h
//  HarborKit
//
//  Created by Joseph Gorecki on 9/3/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HDKHttpResponseHelper : NSObject

+ (BOOL)isInValidRange:(NSURLResponse *)aUrlResponse;

@end
