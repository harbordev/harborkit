//
//  HDKHudHelper.m
//  HarborKit
//
//  Created by Joseph Gorecki on 9/25/14.
//  Copyright (c) 2014 HarborDev. All rights reserved.
//

#import "HDKHudHelper.h"
#import <SVProgressHUD/SVProgressHUD.h>

@implementation HDKHudHelper

+ (void)showWithStatus:(NSString *)aString{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
        
        [SVProgressHUD showWithStatus:aString];
        
    });
    
}

+ (void)showSuccessWithStatus:(NSString *)aString{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
        
        [SVProgressHUD showSuccessWithStatus:aString];
        
    });
    
}

+ (void)showStatusAndMask:(NSString *)aString{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
        
        [SVProgressHUD showWithStatus:aString maskType:SVProgressHUDMaskTypeBlack];
        
    });
    
}

+ (void)showErrorWithStatus:(NSString *)aString{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
        
        [SVProgressHUD showErrorWithStatus:aString];
        
    });
    
}

+ (void)showProgressWithStatus:(NSString *)aString progress:(float)progress{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
        
        [SVProgressHUD showProgress:progress status:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
        
    });
    
}

+ (void)dismiss{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [SVProgressHUD dismiss];
        
    });
    
}

@end

