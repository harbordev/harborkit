//
//  HDKAlertHelper.m
//  HarborKit
//
//  Created by Joseph Gorecki on 9/25/14.
//  Copyright (c) 2014 HarborDev. All rights reserved.
//

#import "HDKAlertHelper.h"

@implementation HDKAlertHelper

+(void)showAlertWithTitle:(NSString *)aTitle andMessage:(NSString *)aMessage{

    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:aTitle message:aMessage delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    
    [alertView show];

}

+ (void)showAlertWithTitle:(NSString *)aTitle andMessage:(NSString *)aMessage fromController:(UIViewController *)viewController{
    
    NSParameterAssert(aTitle);
    NSParameterAssert(aMessage);
    NSParameterAssert(viewController);

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:aTitle message:aMessage preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okay = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:nil];
    
    [alertController addAction:okay];
    
    [viewController presentViewController:alertController animated:YES completion:nil];
    
}

@end
