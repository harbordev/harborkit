//
//  HDKHudHelper.h
//  HarborKit
//
//  Created by Joseph Gorecki on 9/25/14.
//  Copyright (c) 2014 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Helper method on SVProgressHud to show huds
 */

@interface HDKHudHelper : NSObject

+ (void)showWithStatus:(NSString *)aString;
+ (void)showSuccessWithStatus:(NSString *)aString;
+ (void)showStatusAndMask:(NSString *)aString;
+ (void)showErrorWithStatus:(NSString *)aString;
+ (void)showProgressWithStatus:(NSString *)aString progress:(float)progress;
+ (void)dismiss;

@end
