//
//  HDKValidationHelper.h
//  HarborKit
//
//  Created by Joseph Gorecki on 9/25/14.
//  Copyright (c) 2014 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Helper method to validate text.  Password's use Navajo.
 */

@interface HDKValidationHelper : NSObject

/**
 *Text field is not blank
 */
+(BOOL)isValidEntry:(NSString *)aString;

/**
 *Validates as a full email
 */
+(BOOL)isValidEmailAddress:(NSString *)aString;

/**
 *Validates as numbers only
 */
+(BOOL)isValidNumberString:(NSString *)aString;

/**
 *Password is not blank and must be between 4-15 characters
*/
+(BOOL)isValidPasswordSimple:(NSString *)aString;

/**
 *Password is not blank and must be between 4-15 characters, and include a number
 */
+(BOOL)isValidPasswordMedium:(NSString *)aString;

/**
 *Password is not blank, must be between 4-15 characters, must include a number, a symbol, and uppercase
 */
+(BOOL)isValidPasswordComplex:(NSString *)aString;

@end
