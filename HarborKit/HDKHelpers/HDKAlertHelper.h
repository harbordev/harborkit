//
//  HDKAlertHelper.h
//  HarborKit
//
//  Created by Joseph Gorecki on 9/25/14.
//  Copyright (c) 2014 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 *  Helper method to show an single button alert.
 */

@interface HDKAlertHelper : NSObject

+ (void)showAlertWithTitle:(NSString *)aTitle andMessage:(NSString *)aMessage DEPRECATED_MSG_ATTRIBUTE("Uses UIAlertView which is deprecated.");
+ (void)showAlertWithTitle:(NSString *)aTitle andMessage:(NSString *)aMessage fromController:(UIViewController *)viewController;

@end
