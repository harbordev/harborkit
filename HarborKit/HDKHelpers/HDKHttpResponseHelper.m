//
//  HDKHttpResponseHelper.m
//  HarborKit
//
//  Created by Joseph Gorecki on 9/3/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKHttpResponseHelper.h"

@implementation HDKHttpResponseHelper

+ (BOOL)isInValidRange:(NSURLResponse *)aUrlResponse{

    NSHTTPURLResponse *aResponse = (NSHTTPURLResponse *)aUrlResponse;
    
    if(aResponse.statusCode >= 200 && aResponse.statusCode <= 299){
    
        return YES;
    
    }

    //NSLog(@" -- server returned an invalid status code : %ld", (long)aResponse.statusCode);
    
    return NO;
    
}

@end
