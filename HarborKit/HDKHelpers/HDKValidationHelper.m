//
//  HDKValidationHelper.m
//  HarborKit
//
//  Created by Joseph Gorecki on 9/25/14.
//  Copyright (c) 2014 HarborDev. All rights reserved.
//

#import "HDKValidationHelper.h"
#import <UIKit/UIKit.h>
#import <Navajo/NJOPasswordStrengthEvaluator.h>

@implementation HDKValidationHelper

+(BOOL)isValidEntry:(NSString *)aString{
    
    NSString *trimmedString = [aString stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    if(trimmedString.length > 0) {
        
        return YES;
        
    }
    
    return NO;
    
}

+ (BOOL)isValidEmailAddress:(NSString *)aString{
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    return [emailTest evaluateWithObject:aString];
    
}

+(BOOL)isValidNumberString:(NSString *)aString{

    NSArray *failingRules = @[[NJOLengthRule ruleWithRange:NSMakeRange(1, 200)], [NJORequiredCharacterRule decimalDigitCharacterRequiredRule]];
    
    NJOPasswordValidator *validator = [NJOPasswordValidator validatorWithRules:failingRules];
    
    BOOL isValid = [validator validatePassword:aString failingRules:&failingRules];
    
    if (!isValid) {
        
        for (id <NJOPasswordRule> rule in failingRules) {
            
            //NSLog(@"- %@", [rule localizedErrorDescription]);
            
        }
        
    }
    
    return isValid;

}

+ (BOOL)isValidPasswordSimple:(NSString *)aString{
    
    NSArray *failingRules = @[[NJOLengthRule ruleWithRange:NSMakeRange(4, 15)]];
    
    NJOPasswordValidator *validator = [NJOPasswordValidator validatorWithRules:failingRules];
    
    BOOL isValid = [validator validatePassword:aString failingRules:&failingRules];
    
    if (!isValid) {
        
        for (id <NJOPasswordRule> rule in failingRules) {
            
            //NSLog(@"- %@", [rule localizedErrorDescription]);
            
        }
        
    }
    
    return isValid;
    
}

+ (BOOL)isValidPasswordMedium:(NSString *)aString{
    
    NSArray *failingRules = @[[NJOLengthRule ruleWithRange:NSMakeRange(4, 15)], [NJORequiredCharacterRule lowercaseCharacterRequiredRule], [NJORequiredCharacterRule decimalDigitCharacterRequiredRule]];
    
    NJOPasswordValidator *validator = [NJOPasswordValidator validatorWithRules:failingRules];
    
    BOOL isValid = [validator validatePassword:aString failingRules:&failingRules];
    
    if (!isValid) {
        
        for (id <NJOPasswordRule> rule in failingRules) {
            
            //NSLog(@"- %@", [rule localizedErrorDescription]);
            
        }
        
    }
    
    return isValid;
    
}

+ (BOOL)isValidPasswordComplex:(NSString *)aString{
    
    NSArray *failingRules = @[[NJOLengthRule ruleWithRange:NSMakeRange(4, 15)], [NJORequiredCharacterRule lowercaseCharacterRequiredRule], [NJORequiredCharacterRule uppercaseCharacterRequiredRule], [NJORequiredCharacterRule decimalDigitCharacterRequiredRule]];
    
    NJOPasswordValidator *validator = [NJOPasswordValidator validatorWithRules:failingRules];
    
    BOOL isValid = [validator validatePassword:aString failingRules:&failingRules];
    
    if (!isValid) {
        
        for (id <NJOPasswordRule> rule in failingRules) {
            
            //NSLog(@"- %@", [rule localizedErrorDescription]);
            
        }
        
    }
    
    return isValid;
    
}

@end
