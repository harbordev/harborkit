//
//  HDKTableViewCell.h
//  HarborKit
//
//  Created by Joseph Gorecki on 7/16/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HDKTableViewCell : UITableViewCell

+ (UINib *)nib;
- (void)mapToObject:(id)anObject withSelections:(NSSet *)aSet;

@end
