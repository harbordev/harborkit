//
//  HDKCollectionViewCell.m
//  HarborKit
//
//  Created by Joseph Gorecki on 7/16/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKCollectionViewCell.h"

@implementation HDKCollectionViewCell

+ (UINib *)nib{
    
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
    
}

- (void)mapToObject:(id)anObject withSelections:(NSSet *)aSet{
    
    //NSLog(@"sub class me! %s", __PRETTY_FUNCTION__);
    
}

@end
