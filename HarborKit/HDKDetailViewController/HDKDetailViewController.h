//
//  HDKDetailViewController.h
//  HarborKit
//
//  Created by Joseph Gorecki on 8/31/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKBaseViewController.h"

@interface HDKDetailViewController : HDKBaseViewController

@property (strong, nonatomic) Class entityClass;
@property (strong, nonatomic) id entityObject;

+ (instancetype)detailViewControllerForClass:(Class)aClass withInstance:(id)anObject;

+ (instancetype)detailViewControllerForClass:(Class)aClass withId:(NSNumber *)aServerId;

+ (instancetype)detailViewControllerForClass:(Class)aClass withLookup:(NSString *)aLookup andValue:(id)aValue;

+ (instancetype)detailViewControllerForClass:(Class)aClass withDictionary:(NSDictionary *)aDictionary;

- (instancetype)initWithClass:(Class)aClass withObject:(id)anObject;

@end
