//
//  HDKDetailViewController.m
//  HarborKit
//
//  Created by Joseph Gorecki on 8/31/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKDetailViewController.h"
#import <MagicalRecord/MagicalRecord.h>
#import "NSManagedObject+HarborKit.h"

@interface HDKDetailViewController ()

@end

@implementation HDKDetailViewController

+ (instancetype)detailViewControllerForClass:(Class)aClass withInstance:(id)anObject{
    
    return [[self alloc] initWithClass:aClass withObject:anObject];
    
}

+ (instancetype)detailViewControllerForClass:(Class)aClass withId:(NSNumber *)aServerId{
    
    id anObject = [[aClass class] findOrCreateClass:[aClass class] forServer:aServerId inContext:[NSManagedObjectContext MR_defaultContext]];
    
    return [[self alloc] initWithClass:aClass withObject:anObject];

}

+ (instancetype)detailViewControllerForClass:(Class)aClass withLookup:(NSString *)aLookup andValue:(id)aValue{
    
    id anObject = [[self class] MR_findFirstByAttribute:aLookup withValue:aValue];
    
    return [[self alloc] initWithClass:aClass withObject:anObject];

}

+ (instancetype)detailViewControllerForClass:(Class)aClass withDictionary:(NSDictionary *)aDictionary{

    id anObject = [[aClass class] findOrCreateClass:[aClass class] forServer:@([aDictionary[@"id"] integerValue]) inContext:[NSManagedObjectContext MR_defaultContext]];
    
    return [[self alloc] initWithClass:aClass withObject:anObject];

}

- (instancetype)initWithClass:(Class)aClass withObject:(id)anObject{

    self = [super init];
    
    if(self){
        
        _entityClass = aClass;
        _entityObject = anObject;
        
    }
    
    return self;

}

@end
