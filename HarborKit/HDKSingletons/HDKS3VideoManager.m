//
//  ZPTS3VideoManager.m
//  ZipIt
//
//  Created by Joseph Gorecki on 10/5/15.
//  Copyright © 2015 harbordev. All rights reserved.
//

#import "HDKS3VideoManager.h"
#import <AWSCore/AWSCore.h>
#import <AWSS3/AWSS3.h>

#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <MobileCoreServices/UTCoreTypes.h>

#import "HDKHudHelper.h"

@implementation HDKS3VideoManager

+ (HDKS3VideoManager *)sharedInstance{
    
    static HDKS3VideoManager * sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[[self class] alloc] init];
        
        
    });
    
    return sharedInstance;
    
}

#pragma mark - its a video
- (BOOL)fileisVideo:(NSString *)mediaType{
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeAudiovisualContent]) {
        
        return YES;
        
    }
    
    return NO;
    
}

#pragma mark - play video
- (void)playVideo:(NSURL *)aUrl inViewController:(UIViewController *)aVC{
    
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
    
    AVPlayer *player = [AVPlayer playerWithURL:aUrl];
    
    playerViewController.player = player;
    
    [playerViewController.player play];
    
    [aVC presentViewController:playerViewController animated:YES completion:nil];
    
    //aVC.na
    
}

#pragma mark - thumbnail for video
- (UIImage *)generateImage:(NSString *)videoPath{
    
    NSURL *url = [NSURL fileURLWithPath:videoPath];
    
    AVURLAsset *asset = [AVURLAsset assetWithURL:url];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    
    CMTime time = [asset duration];
    time.value = 2;
    
    __block UIImage *anImage = nil;
    
    NSError *err = nil;
    
    CGImageRef imageRef = [generator copyCGImageAtTime:time actualTime:NULL error:&err];
    
    if (err) {
        
        //NSLog(@"%s, %@", __PRETTY_FUNCTION__, err);
        
    }else{
        
        [HDKHudHelper showStatusAndMask:nil];
        
        anImage = [UIImage imageWithCGImage:imageRef];
        
        CGImageRelease(imageRef);
        
        [HDKHudHelper dismiss];
        
        //NSLog(@"did create thumbnailImage");
        
    }
    
    //NSLog(@"anImage: %@", anImage);
    
    return anImage;
    
}


- (NSData *)dataForVideoAtFilePath:(NSString *)aFilePath{
    
    return [NSData dataWithContentsOfFile:aFilePath];
    
}

- (NSString *)nameForVideo:(NSString *)aUniqueFileName{
    
    return [NSString stringWithFormat:@"%@/%@", self.staticFilesBase, aUniqueFileName];
    
}

- (void)uploadVideoAsDataToS3:(NSData *)aData withFileName:(NSString *)aFileName withCompletion:(HDKCompletionBlock)completion{
    
    AWSS3TransferUtilityUploadExpression *expression = [AWSS3TransferUtilityUploadExpression new];
    
//    expression.uploadProgress = ^(AWSS3TransferUtilityTask *task, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//
//            float progress = totalBytesSent / totalBytesExpectedToSend;
//
//            //NSLog(@"progress: %f", progress);
//
//            [HDKHudHelper showProgressWithStatus:@"Loading" progress:progress];
//            
//        });
//
//    };
    
    AWSS3TransferUtilityUploadCompletionHandlerBlock completionHandler = ^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [HDKHudHelper dismiss];
            
            if(error){
                
                [HDKHudHelper showErrorWithStatus:error.localizedDescription];
                
            }else{
                
                [HDKHudHelper showSuccessWithStatus:@"Loaded Video"];
                
                if(completion){
                    
                    completion();
                    
                }
                
            }
            
        });
        
    };
    
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
    
    [[transferUtility uploadData:aData bucket:self.bucketName key:aFileName contentType:@"video/mov" expression:expression completionHander:completionHandler] continueWithBlock:^id(AWSTask *task) {
        
        //NSLog(@"aws task.result: %@", task);
        
        if (task.error) {
            
            //NSLog(@"Error: %@", task.error);
            
        }
        
        if (task.exception) {
            
            //NSLog(@"Exception: %@", task.exception);
            
        }
        
        if (task.result) {
            
            AWSS3TransferUtilityUploadTask *uploadTask = task.result;
            
            //NSLog(@"upload task: %@", uploadTask);
            
            // Do something with uploadTask.
        }
        
        return nil;
    }];
    
    
}

@end
