//
//  ZPTLocationManager.m
//  ZipIt
//
//  Created by Joseph Gorecki on 7/22/15.
//  Copyright (c) 2015 harbordev. All rights reserved.
//

#import "HDKLocationManager.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface HDKLocationManager ()<CLLocationManagerDelegate>

@property (nonatomic, strong) CLPlacemark *placemark;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, strong) CLLocationManager *locationManager;

@end

static NSInteger locationDistanceFilter = 65;

@implementation HDKLocationManager

+ (HDKLocationManager *)sharedInstance{
    
    static HDKLocationManager * sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[[self class] alloc] init];

    });
    
    return sharedInstance;
    
}

- (id)init{
    
    self = [super init];
    
    if(self){
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.distanceFilter = kCLLocationAccuracyHundredMeters;
        _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationEnteringBackground:)
                                                     name:UIApplicationWillResignActiveNotification object:nil];
        
    }
    
    return self;
    
}

- (void)requestAuthorization{

    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [_locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    [self.locationManager stopUpdatingLocation];

}

- (void)run:(HDKLocationManagerCompletion)completion{

    if( self.locationManager )
    
    [self.locationManager startUpdatingLocation];
    
    if(completion){
    
        completion();
    
    }
    
}

- (void)stop{
    
    ////NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [self.locationManager stopUpdatingLocation];
    
}

#pragma mark - location manager
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    self.location = [locations lastObject];
    
    if([self coordinateIsValid:self.location]){
        
       // //NSLog(@"-- using this location: %@", self.location);
        
        CLGeocoder *geocoder = [CLGeocoder new];
        
        [geocoder reverseGeocodeLocation:self.location completionHandler:^(NSArray *placemarks, NSError *error) {
        
            if (error || placemarks.count == 0) {

                //NSLog(@"geocoding error: %@", error.localizedDescription);
                
            }else{
                
                self.placemark = [placemarks objectAtIndex:0];
                
                if([self.delegate respondsToSelector:@selector(didUpDateLocation)]){
                
                    dispatch_async(dispatch_get_main_queue(), ^{
                    
                        [self.delegate didUpDateLocation];
                        
                    });
                    
                }
                
            }
        }];
        
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    //NSLog(@"%s : %@", __PRETTY_FUNCTION__, error);
    
    [self stop];
    
}

#pragma mark - CoreLocation helpers
- (BOOL)canLocateDevice{
    
    CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
    
    if (([CLLocationManager locationServicesEnabled] == NO) || (authorizationStatus == kCLAuthorizationStatusDenied) || (authorizationStatus == kCLAuthorizationStatusRestricted)) {
        
        //NSLog(@"unable to locate device");
        
        return NO;
        
    }
    
    return YES;
    
}

- (BOOL)coordinateIsValid:(CLLocation *)aLocation{
    
    if (aLocation == nil ||
        //[[NSDate date] timeIntervalSinceDate:aLocation.timestamp] >= locationTimeStamp ||
        aLocation.horizontalAccuracy > locationDistanceFilter ||
        !CLLocationCoordinate2DIsValid(aLocation.coordinate)){
        
        return NO;
        
    }
    
    return YES;
}

#pragma mark - UIApplicationState
- (void)applicationEnteringBackground:(NSNotification *)notification{

    [self.locationManager stopUpdatingLocation];

}

@end
