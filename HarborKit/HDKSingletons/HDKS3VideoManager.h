//
//  ZPTS3VideoManager.h
//  ZipIt
//
//  Created by Joseph Gorecki on 10/5/15.
//  Copyright © 2015 harbordev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HDKDataSourceConstants.h"

@class MPMoviePlayerController;

@interface HDKS3VideoManager : NSObject

+ (HDKS3VideoManager *)sharedInstance;

@property (copy, nonatomic) NSString *bucketName;
@property (copy, nonatomic) NSString *staticFilesBase;

- (BOOL)fileisVideo:(NSString *)mediaType;
- (UIImage *)generateImage:(NSString *)videoPath;

- (void)playVideo:(NSURL *)aUrl inViewController:(UIViewController *)aVC;

- (NSData *)dataForVideoAtFilePath:(NSString *)aFilePath;
- (NSString *)nameForVideo:(NSString *)aUniqueFileName;

- (void)uploadVideoAsDataToS3:(NSData *)aData withFileName:(NSString *)aFileName withCompletion:(HDKCompletionBlock)completion;

@end



