//
//  HDKFacebookManager.m
//  HarborKit
//
//  Created by Joseph Gorecki on 11/24/15.
//  Copyright © 2015 HarborDev. All rights reserved.
//

#import "HDKFacebookManager.h"

@import AssetsLibrary;
@import AVFoundation;
@import AVKit;
@import Photos;

@protocol HDKFacebookManager <NSObject>

@end

@implementation HDKFacebookManager

+ (HDKFacebookManager *)sharedInstance{
    
    static HDKFacebookManager * sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[[self class] alloc] init];
        
    });
    
    return sharedInstance;
    
}

- (id)init{
    
    self = [super init];
    
    if(self){
     
        [FBSDKAppEvents activateApp];
        
    }
    
    return self;
    
}

#pragma mark - Facebook login methods
- (BOOL)userIsLoggedIn{
    
    //NSLog(@"not implemented yet");
    
    return NO;

}

- (void)userShouldLogOut:(HDKFacebookManagerCompletion)completion error:(HDKFacebookManagerCompletion)error{

    //NSLog(@"not implemented yet");
    
}

#pragma mark - FBSDKLoginButtonDelegate
- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error{



}

#pragma mark - Facebook sharing methods
- (void)shareLinkToFacebook:(NSURL *)aSharedURL title:(NSString *)aTitle description:(NSString *)aDescription fromViewController:(UIViewController *)aVC completion:(HDKFacebookManagerCompletion)completion{

    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = aSharedURL;
    content.contentTitle = aTitle;
    content.contentDescription = aDescription;
    
    FBSDKShareDialog * dialog = [[FBSDKShareDialog alloc] init];
    dialog.delegate = self;
    
    if(dialog.canShow){
    
        dialog.fromViewController = aVC;
        dialog.shareContent = content;
        dialog.mode = FBSDKShareDialogModeAutomatic;
        
        [dialog show];
    
    }

}

- (void)shareImageToFacebook:(UIImage *)anImage title:(NSString *)aTitle fromViewController:(UIViewController *)aVC completion:(HDKFacebookManagerCompletion)completion{
    
    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    photo.image = anImage;
    photo.userGenerated = YES;
    photo.caption = aTitle;
    
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    content.photos = @[photo];
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.delegate = self;
    
    if(dialog.canShow){
        
        dialog.fromViewController = aVC;
        dialog.shareContent = content;
        dialog.mode = FBSDKShareDialogModeNative;
        
        [dialog show];
        
    }else{
        
        //NSLog(@"%s couldn not show the dialog box?", __PRETTY_FUNCTION__);
        
    }
    
}


- (void)shareVideoToFacebook:(NSURL *)aFileURL fromViewController:(UIViewController *)aVC completion:(HDKFacebookManagerCompletion)completion{

    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    [library writeVideoAtPathToSavedPhotosAlbum:aFileURL completionBlock:^(NSURL *assetURL, NSError *error){

        if(!error){
        
            FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
            video.videoURL = assetURL;
            
            FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
            content.video = video;
            
            FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
            dialog.delegate = self;
            
            if(dialog.canShow){
                
                dialog.fromViewController = aVC;
                dialog.shareContent = content;
                dialog.mode = FBSDKShareDialogModeNative;
                
                [dialog show];
                
            }else{
            
                //NSLog(@"the dialog couldn't be shown");
            
            }
            
        }else{
        
            //NSLog(@"an error happend on the save to the saved photo album: %@", error);
        
        }
        
    }];
    
}

#pragma mark - FBSDKSharingDelegate
/*!
 @abstract Sent to the delegate when the share completes without error or cancellation.
 @param sharer The FBSDKSharing that completed.
 @param results The results from the sharer.  This may be nil or empty.
 */
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results{

    //NSLog(@"%s completed %@", __PRETTY_FUNCTION__, results);

}

/*!
 @abstract Sent to the delegate when the sharer encounters an error.
 @param sharer The FBSDKSharing that completed.
 @param error The error.
 */
- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error{

    //NSLog(@"%s error %@", __PRETTY_FUNCTION__, error);

}

/*!
 @abstract Sent to the delegate when the sharer is cancelled.
 @param sharer The FBSDKSharing that completed.
 */
- (void)sharerDidCancel:(id<FBSDKSharing>)sharer{

    //NSLog(@"%s canceled %@", __PRETTY_FUNCTION__, sharer);

}

@end
