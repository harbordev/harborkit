//
//  HDKDateManager.m
//  HDK
//
//  Created by Joseph Gorecki on 11/11/14.
//  Copyright (c) 2014 HarborDev. All rights reserved.
//

#import "HDKDateManager.h"

@implementation HDKDateManager

+ (HDKDateManager *)sharedInstance{
    
    static HDKDateManager * sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[[self class] alloc] init];
        
    });
    
    return sharedInstance;
    
}

- (id)init{
    
    self = [super init];
    
    if(self){
        
        _formatter = [[NSDateFormatter alloc] init];
        _formatter.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        _formatter.dateStyle = NSDateFormatterFullStyle;
        
    }
    
    return self;
    
}

- (NSDate *)formatFromStringWithT:(NSString *)aString{
    
    NSArray *anArray = [aString componentsSeparatedByString:@"."];
    
    self.formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
    
    NSDate *aDate = [self.formatter dateFromString:[anArray firstObject]];
    
    return aDate;
}

- (NSDate *)formatFromString:(NSString *)aString{
    
    self.formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSDate *aDate = [self.formatter dateFromString:aString];
    
    return aDate;
}

- (NSString *)formatedDateStringFromString:(NSString *)aString{

    NSDate *aDate = [self formatFromString:aString];
    
    self.formatter.dateFormat = @"dd/MM/yy";
    
    return [self.formatter stringFromDate:aDate];

}

- (NSString *)formatedDateStringFromTString:(NSString *)aString{
    
    NSDate *aDate = [self formatFromStringWithT:aString];
    
    self.formatter.dateFormat = @"MM/dd/yy";
    
    return [self.formatter stringFromDate:aDate];
    
}

@end
