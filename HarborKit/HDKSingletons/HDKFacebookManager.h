//
//  HDKFacebookManager.h
//  HarborKit
//
//  Created by Joseph Gorecki on 11/24/15.
//  Copyright © 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

typedef void(^HDKFacebookManagerCompletion)(void);

@interface HDKFacebookManager : NSObject<FBSDKLoginButtonDelegate, FBSDKSharingDelegate>

+ (HDKFacebookManager *)sharedInstance;

- (BOOL)userIsLoggedIn;
- (void)userShouldLogOut:(HDKFacebookManagerCompletion)completion error:(HDKFacebookManagerCompletion)error;

- (void)shareLinkToFacebook:(NSURL *)aSharedURL title:(NSString *)aTitle description:(NSString *)aDescription fromViewController:(UIViewController *)aVC completion:(HDKFacebookManagerCompletion)completion;
- (void)shareImageToFacebook:(UIImage *)anImage title:(NSString *)aTitle fromViewController:(UIViewController *)aVC completion:(HDKFacebookManagerCompletion)completion;
- (void)shareVideoToFacebook:(NSURL *)aFileURL fromViewController:(UIViewController *)aVC completion:(HDKFacebookManagerCompletion)completion;

/*
 
 https://developers.facebook.com/docs/sharing/ios
 
 <key>NSAppTransportSecurity</key>
 <dict>
 <key>NSExceptionDomains</key>
 <dict>
 <key>facebook.com</key>
 <dict>
 <key>NSIncludesSubdomains</key>
 <true/>
 <key>NSThirdPartyExceptionRequiresForwardSecrecy</key>
 <false/>
 </dict>
 <key>fbcdn.net</key>
 <dict>
 <key>NSIncludesSubdomains</key>
 <true/>
 <key>NSThirdPartyExceptionRequiresForwardSecrecy</key>
 <false/>
 </dict>
 <key>akamaihd.net</key>
 <dict>
 <key>NSIncludesSubdomains</key>
 <true/>
 <key>NSThirdPartyExceptionRequiresForwardSecrecy</key>
 <false/>
 </dict>
 </dict>
 </dict>
 
 <key>LSApplicationQueriesSchemes</key>
 <array>
 <string>fbapi</string>
 <string>fbapi20130214</string>
 <string>fbapi20130410</string>
 <string>fbapi20130702</string>
 <string>fbapi20131010</string>
 <string>fbapi20131219</string>
 <string>fbapi20140410</string>
 <string>fbapi20140116</string>
 <string>fbapi20150313</string>
 <string>fbapi20150629</string>
 <string>fbauth</string>
 <string>fbauth2</string>
 <string>fb-messenger-api20140430</string>
 </array>
 
 */

@end
