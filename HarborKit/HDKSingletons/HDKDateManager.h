//
//  HDKDateManager.m
//  HDK
//
//  Created by Joseph Gorecki on 11/11/14.
//  Copyright (c) 2014 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HDKDateManager : NSObject

/**
 * Singleton for date formatting server responses.
 */
+ (HDKDateManager *)sharedInstance;

@property (strong, nonatomic) NSDateFormatter *formatter;

- (NSDate *)formatFromStringWithT:(NSString *)aString;
- (NSDate *)formatFromString:(NSString *)aString;
- (NSString *)formatedDateStringFromString:(NSString *)aString;
- (NSString *)formatedDateStringFromTString:(NSString *)aString;

@end
