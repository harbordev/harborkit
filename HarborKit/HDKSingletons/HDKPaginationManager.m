//
//  HDKPaginationManager.m
//  HarborKit
//
//  Created by Joseph Gorecki on 12/10/15.
//  Copyright © 2015 HarborDev. All rights reserved.
//

#import "HDKPaginationManager.h"
#import <ObjectiveSugar/ObjectiveSugar.h>

@interface HDKPaginationManager ()

@property (strong, nonatomic) NSMutableDictionary *storage;

@end

@implementation HDKPaginationManager

+ (HDKPaginationManager *)sharedInstance{
    
    static HDKPaginationManager * sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[[self class] alloc] init];
        
    });
    
    return sharedInstance;
    
}

-(id)init{
    
    self = [super init];
    
    if(self){
        
        _storage = [[NSMutableDictionary alloc] init];
        
    }
    
    return self;
    
}

- (BOOL)shouldLoadNextSet:(NSDictionary *)aDict{
    
    if(aDict == nil){
        
        ////NSLog(@"meta dictionary is nil - no pagination is set up.");
        
        return NO;
        
    }
    
    if (![aDict[@"next"] isKindOfClass:[NSNull class]]) {
        
        ////NSLog(@"meta dictionary is loading next set.");
        
        return YES;
        
    }
    
    return NO;
    
}

- (void)storeMetaFromJson:(id)json withName:(NSString *)name{
    
    if(![json isKindOfClass:[NSDictionary class]]){
        
        ////NSLog(@"json is not a dictionary - so there is no pagination meta dictionary");
        
        return;
        
    }
    
    if([json hasKey:@"meta"] ){
        
        [self.storage setObject:json[@"meta"] forKey:name];
        
    }else{
        
        ////NSLog(@"json has no meta dictionary to save");
        
    }
    
}

- (NSDictionary *)metaDictionaryForName:(NSString *)name{
    
    return [[self.storage objectForKey:name] copy];
    
}

- (NSDictionary *)createPaginationDictionary:(NSDictionary *)currentParameters offset:(NSUInteger)newOffset limit:(NSUInteger)newLimit{
    
    NSMutableDictionary *aDict = [currentParameters mutableCopy];
    
    [aDict setObject:@(newOffset) forKey:@"offset"];
    [aDict setObject:@(newLimit) forKey:@"limit"];
    
    return aDict;
    
}

@end
