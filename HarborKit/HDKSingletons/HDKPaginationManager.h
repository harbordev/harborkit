//
//  HDKPaginationManager.h
//  HarborKit
//
//  Created by Joseph Gorecki on 12/10/15.
//  Copyright © 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HDKPaginationManager : NSObject

/**
 * Singleton for storing meta from api requests -- required for pagination
 */
+ (HDKPaginationManager *)sharedInstance;

/**
 * Checks to see if the stored dictionary is valid and wants to load Next
 */
- (BOOL)shouldLoadNextSet:(NSDictionary *)aDict;

/**
 * Creates a dictinary based on a name -- adds it to the storage dictionary
 */
- (void)storeMetaFromJson:(id)json withName:(NSString *)name;

/**
 * Get a dictionary for pagination processing
 */
- (NSDictionary *)metaDictionaryForName:(NSString *)name;

/**
 * Helper to update the pagination dictionary for the next server request
 */
- (NSDictionary *)createPaginationDictionary:(NSDictionary *)currentParameters offset:(NSUInteger)newOffset limit:(NSUInteger)newLimit;

@end
