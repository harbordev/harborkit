//
//  ZPTLocationManager.h
//  ZipIt
//
//  Created by Joseph Gorecki on 7/22/15.
//  Copyright (c) 2015 harbordev. All rights reserved.
//

#import <Foundation/Foundation.h>

@import CoreLocation;
@import UIKit;

typedef void(^HDKLocationManagerCompletion)(void);

@protocol HDKLocationManagerDelegate <NSObject>

- (void)didUpDateLocation;

@end

@interface HDKLocationManager : NSObject

@property (nonatomic, strong, readonly) CLPlacemark *placemark;
@property (nonatomic, strong, readonly) CLLocation *location;
@property (nonatomic, weak) id<HDKLocationManagerDelegate>delegate;

+ (HDKLocationManager *)sharedInstance;

- (void)requestAuthorization;
- (void)run:(HDKLocationManagerCompletion)completion;
- (void)stop;

@end
