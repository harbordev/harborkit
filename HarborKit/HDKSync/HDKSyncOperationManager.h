//
//  HDKSyncOperationManager.h
//  HarborKit
//
//  Created by Joseph Gorecki on 7/23/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

@import Foundation;
@import CoreData;
@import UIKit;

@protocol HDKSyncOperationManagerDelegate <NSObject>

- (void)operationsDidComplete;

@end

@interface HDKSyncOperationManager : NSObject

+ (HDKSyncOperationManager *)sharedInstance;

@property (strong, nonatomic) NSOperationQueue *queue;
@property (strong, nonatomic) id<HDKSyncOperationManagerDelegate>delegate;

- (void)addOperationToQueue:(NSBlockOperation *)operation;
- (void)start;
- (void)stop;

@end
