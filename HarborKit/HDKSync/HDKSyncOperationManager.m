//
//  HDKSyncOperationManager.m
//  HarborKit
//
//  Created by Joseph Gorecki on 7/23/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKSyncOperationManager.h"

@implementation HDKSyncOperationManager

+ (HDKSyncOperationManager *)sharedInstance{
    
    static HDKSyncOperationManager * sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[[self class] alloc] init];
    
    });
    
    return sharedInstance;
    
}

-(id)init{
    
    self = [super init];
    if(self){
        
        _queue = [[NSOperationQueue alloc] init];
        _queue.maxConcurrentOperationCount = 1;
        [_queue setSuspended:YES];
        
        [_queue addObserver:self forKeyPath:@"operationCount" options:NSKeyValueObservingOptionNew context:nil];
        
        
    }
    return self;
    
    
}

- (void)addOperationToQueue:(NSBlockOperation *)operation{

    [self.queue addOperation:operation];

}

- (void)start{

    [self.queue setSuspended:NO];

}

- (void)stop{

    [self.queue cancelAllOperations];
    [self.queue setSuspended:YES];

}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{

    ////NSLog(@"object: %@ : %@", [object class], keyPath);
    
    if ([keyPath isEqualToString:@"operationCount"]){
        
        if (self.queue.operationCount == 0){
            
            [self.queue setSuspended:YES];
            [self.delegate operationsDidComplete];
            
            ////NSLog(@"operations completed.");
            
        //}else{
            
            ////NSLog(@"--- operation completed, but operations remain: %@", keyPath);
            
        }
        
    }
    
}

@end
