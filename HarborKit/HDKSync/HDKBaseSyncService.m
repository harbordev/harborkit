//
//  HDKBaseSyncService.m
//  HarborKit
//
//  Created by Joseph Gorecki on 7/23/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKBaseSyncService.h"

#import "HDKSyncOperationManager.h"
#import "HDKPaginationManager.h"

#import "NSMutableURLRequest+HarborKit.h"
#import "NSManagedObject+HarborKit.h"

#import "HDKHttpResponseHelper.h"

#import <MagicalRecord/MagicalRecord.h>

@interface HDKBaseSyncService ()<HDKSyncOperationManagerDelegate>

@end

@implementation HDKBaseSyncService

- (void)getObjects:(Class)aClass fromServerWithDict:(NSDictionary *)aDict path:(NSString *)aPath withRelated:(id)obj andCompletion:(HDKCompletionBlock)completion andError:(HDKCompletionBlock)errorBlock{
    
    NSParameterAssert([aDict isKindOfClass:[NSDictionary class]]);
    
    NSBlockOperation *blockOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
        });
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:[NSMutableURLRequest hdk_requestForGET:aPath withParameters:aDict] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(!error && [HDKHttpResponseHelper isInValidRange:response]){
                
                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                [[HDKPaginationManager sharedInstance] storeMetaFromJson:json withName:NSStringFromClass([aClass class])];
                
                [[aClass class] parseJSONIntoDataStore:json withRelated:obj andCompletion:(HDKCompletionBlock)completion];
                
            }else{
                
                ////NSLog(@"%s : %@", __PRETTY_FUNCTION__, error.localizedDescription);
                
                if(errorBlock){
                    
                    errorBlock();
                    
                }
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
            });
            
        }];
        
        [task resume];
        
    }];
    
    [HDKSyncOperationManager sharedInstance].delegate = self;
    
    [[HDKSyncOperationManager sharedInstance] addOperationToQueue:blockOperation];
    
    [[HDKSyncOperationManager sharedInstance] start];
    
}

- (void)createNewObject:(Class)aClass withParameters:(NSDictionary *)aDict path:(NSString *)aPath withRelated:(id)obj andCompletion:(HDKCreatedCompletionBlock)completion andError:(HDKCompletionBlock)errorBlock{
    
    NSParameterAssert([aDict isKindOfClass:[NSDictionary class]]);
    
    NSBlockOperation *blockOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
        });
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:[NSMutableURLRequest hdk_requestForPOST:aPath withParameters:aDict] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(!error && [HDKHttpResponseHelper isInValidRange:response]){
                
                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                [[aClass class] createdObjectResponse:json andRelatedId:obj andCompletion:completion];
                
            }else{
                
                ////NSLog(@"%s : %@", __PRETTY_FUNCTION__, error.localizedDescription);
                
                if(errorBlock){
                    
                    errorBlock();
                    
                }
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
            });
            
        }];
        
        [task resume];
        
    }];
    
    [HDKSyncOperationManager sharedInstance].delegate = self;
    
    [[HDKSyncOperationManager sharedInstance] addOperationToQueue:blockOperation];
    
    [[HDKSyncOperationManager sharedInstance] start];
    
}

- (void)createNewObjectWithImage:(Class)aClass withParameters:(NSDictionary *)aDict withImage:(UIImage *)anImage path:(NSString *)aPath withRelated:(id)obj andCompletion:(HDKCreatedCompletionBlock)completion andError:(HDKCompletionBlock)errorBlock{
    
    NSParameterAssert([aDict isKindOfClass:[NSDictionary class]]);
    
    NSBlockOperation *blockOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
        });
        
        NSMutableURLRequest *aRequest = [NSMutableURLRequest hdk_requestForMultiform:aPath withImage:anImage withID:nil withInputName:@"image" withJson:aDict];
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:aRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(!error && [HDKHttpResponseHelper isInValidRange:response]){
                
                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                [[aClass class] createdObjectResponse:json andRelatedId:obj andCompletion:completion];
                
            }else{
                
                ////NSLog(@"%s : %@", __PRETTY_FUNCTION__, error.localizedDescription);
                
                if(errorBlock){
                    
                    errorBlock();
                    
                }
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
            });
            
        }];
        
        [task resume];
        
    }];
    
    [HDKSyncOperationManager sharedInstance].delegate = self;
    
    [[HDKSyncOperationManager sharedInstance] addOperationToQueue:blockOperation];
    
    [[HDKSyncOperationManager sharedInstance] start];
    
}

- (void)updateObject:(Class)aClass withInstance:(id)anObject withParameters:(NSDictionary *)aDict atPath:(NSString *)aPath withRelated:(id)relatedObject andCompletion:(HDKUpdatedCompletionBlock)completion andError:(HDKCompletionBlock)errorBlock{
    
    NSParameterAssert([aDict isKindOfClass:[NSDictionary class]]);
    
    NSBlockOperation *blockOperation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
        });
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:[NSMutableURLRequest hdk_requestForPOST:aPath withParameters:aDict] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(!error && [HDKHttpResponseHelper isInValidRange:response]){
                
                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                if(completion){
                    
                    completion(json);
                    
                }
                
            }else{
                
                ////NSLog(@"%s : %@", __PRETTY_FUNCTION__, error.localizedDescription);
                
                if(errorBlock){
                    
                    errorBlock();
                    
                }
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
            });
            
        }];
        
        [task resume];
        
    }];
    
    [HDKSyncOperationManager sharedInstance].delegate = self;
    
    [[HDKSyncOperationManager sharedInstance] addOperationToQueue:blockOperation];
    
    [[HDKSyncOperationManager sharedInstance] start];
    
    
}

#pragma mark - HDKSyncOperationManagerDelegate
- (void)operationsDidComplete{
    
    ////NSLog(@"completed all sync operations");
    
    if([self.delegate respondsToSelector:@selector(syncServiceDidComplete)]){
        
        [self.delegate syncServiceDidComplete];
        
    }
    
}

@end

