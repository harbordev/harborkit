//
//  HDKModelable.h
//  HarborKit
//
//  Created by Joseph Gorecki on 8/31/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HDKModelable <NSObject>

@end
