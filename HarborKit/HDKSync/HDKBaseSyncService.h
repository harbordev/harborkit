//
//  HDKBaseSyncService.h
//  HarborKit
//
//  Created by Joseph Gorecki on 7/23/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

@import Foundation;
@import CoreData;
@import UIKit;

#import "HDKDataSourceConstants.h"

@protocol HDKBaseSyncServiceDelegate <NSObject>

- (void)syncServiceDidComplete;

@end

@interface HDKBaseSyncService : NSObject

@property (strong, nonatomic) Class entityClass;
@property (strong, nonatomic) NSManagedObjectContext *moc;
@property (weak, nonatomic) id<HDKBaseSyncServiceDelegate>delegate;

- (void)getObjects:(Class)aClass fromServerWithDict:(NSDictionary *)aDict path:(NSString *)aPath withRelated:(id)obj andCompletion:(HDKCompletionBlock)completion andError:(HDKCompletionBlock)errorBlock;

- (void)createNewObject:(Class)aClass withParameters:(NSDictionary *)aDict path:(NSString *)aPath withRelated:(id)obj andCompletion:(HDKCreatedCompletionBlock)completion andError:(HDKCompletionBlock)errorBlock;

- (void)createNewObjectWithImage:(Class)aClass withParameters:(NSDictionary *)aDict withImage:(UIImage *)anImage path:(NSString *)aPath withRelated:(id)obj andCompletion:(HDKCreatedCompletionBlock)completion andError:(HDKCompletionBlock)errorBlock;

- (void)updateObject:(Class)aClass withInstance:(id)anObject withParameters:(NSDictionary *)aDict atPath:(NSString *)aPath withRelated:(id)relatedObject andCompletion:(HDKUpdatedCompletionBlock)completion andError:(HDKCompletionBlock)errorBlock;

@end
