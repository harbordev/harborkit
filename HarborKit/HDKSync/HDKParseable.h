//
//  HDKParseable.h
//  HarborKit
//
//  Created by Joseph Gorecki on 8/22/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HDKDataSourceConstants.h"

@protocol HDKParseable <NSObject>

+ (NSString *)requestURLForModelList;

@required;

+ (void)parseJSONIntoDataStore:(id)json withRelated:(id)obj andCompletion:(HDKCompletionBlock)completion;

@end
