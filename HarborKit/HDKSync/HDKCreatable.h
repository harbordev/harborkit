//
//  HDKCreatable.h
//  HarborKit
//
//  Created by Joseph Gorecki on 8/22/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HDKDataSourceConstants.h"

@protocol HDKCreatable <NSObject>

@required;

+ (NSString *)requestURLForModelCreation;

+ (NSDictionary *)createDicionaryForServer:(NSDictionary *)aStorageDictionary;

+ (void)createdObjectResponse:(NSDictionary *)aServerResponse andRelatedId:(id)anObject andCompletion:(HDKCreatedCompletionBlock)completion;

@end
