//
//  NSMutableURLRequest+HarborKit.h
//  HarborKit
//
//  Created by Joseph Gorecki on 7/13/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSMutableURLRequest (HarborKit)

+ (NSMutableURLRequest *)hdk_requestForPOST:(NSString *)aPath withParameters:(NSDictionary *)aDictionary;
+ (NSMutableURLRequest *)hdk_requestForMultiform:(NSString *)aPath withImage:(UIImage *)anImage withID:(NSNumber *)aNumber withInputName:(NSString *)anInputName withJson:(NSDictionary *)aDict;
+ (NSMutableURLRequest *)hdk_requestForGET:(NSString *)aPath withParameters:(NSDictionary *)aDictionary;

@end
