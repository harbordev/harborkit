//
//  NSMutableURLRequest+HarborKit.m
//  HarborKit
//
//  Created by Joseph Gorecki on 7/13/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "NSMutableURLRequest+HarborKit.h"
#import "HDKUserObject.h"
#import "NSDictionary+HDK.h"
#import <OMGHTTPURLRQ/OMGHTTPURLRQ.h>
#import <FormatterKit/TTTURLRequestFormatter.h>

@implementation NSMutableURLRequest (HarborKit)

+ (NSMutableURLRequest *)hdk_requestForPOST:(NSString *)aPath withParameters:(NSDictionary *)aDictionary{
    
    NSMutableURLRequest *request = [OMGHTTPURLRQ POST:aPath JSON:aDictionary];
    
    HDKUserObject *aUserObject = [HDKUserObject getUserObjectFromDefaults];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"json" forHTTPHeaderField:@"format"];
    [request addValue:[NSString stringWithFormat:@"ApiKey %@:%@", aUserObject.username, aUserObject.key] forHTTPHeaderField:@"Authorization"];
    
    //NSLog(@"%s: Request: %@", __PRETTY_FUNCTION__, [TTTURLRequestFormatter cURLCommandFromURLRequest:request]);
    
    return request;
    
}

+ (NSMutableURLRequest *)hdk_requestForMultiform:(NSString *)aPath withImage:(UIImage *)anImage withID:(NSNumber *)aNumber withInputName:(NSString *)anInputName withJson:(NSDictionary *)aDict{
    
    NSParameterAssert(anInputName.length > 0);
    NSParameterAssert([aDict isKindOfClass:[NSDictionary class]]);
    
    HDKUserObject *aUserObject = [HDKUserObject getUserObjectFromDefaults];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:aPath]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    [request addValue:[NSString stringWithFormat:@"ApiKey %@:%@", aUserObject.username, aUserObject.key] forHTTPHeaderField:@"Authorization"];
    
    NSMutableData *body = [NSMutableData data];
    
    if(anImage){
    
        NSString *uniqueName = [NSString stringWithFormat:@"%@.jpg", [[NSProcessInfo processInfo] globallyUniqueString]];
        NSData *data = UIImageJPEGRepresentation(anImage, 80);
    
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", anInputName, uniqueName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:data]];
    
    }else{
    
        //NSLog(@"%s optional image not set in multi-form.", __PRETTY_FUNCTION__);
    
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"json\"\r\n\r\n%@", [aDict hdk_jsonString]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    //NSLog(@"%s: Request: %@", __PRETTY_FUNCTION__, [TTTURLRequestFormatter cURLCommandFromURLRequest:request]);
    
    return request;
    
}

+ (NSMutableURLRequest *)hdk_requestForGET:(NSString *)aPath withParameters:(NSDictionary *)aDictionary{
    
    NSMutableURLRequest *request = [OMGHTTPURLRQ GET:aPath :aDictionary];
    
    HDKUserObject *aUserObject = [HDKUserObject getUserObjectFromDefaults];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"json" forHTTPHeaderField:@"format"];
    [request addValue:[NSString stringWithFormat:@"ApiKey %@:%@", aUserObject.username, aUserObject.key] forHTTPHeaderField:@"Authorization"];
    
    //NSLog(@"%s: Request: %@", __PRETTY_FUNCTION__, [TTTURLRequestFormatter cURLCommandFromURLRequest:request]);    
    
    return request;
    
}


@end
