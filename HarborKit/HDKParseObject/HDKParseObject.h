//
//  HDKParseObject.h
//  Favement
//
//  Created by Joseph Gorecki on 11/2/15.
//  Copyright © 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@import UIKit;

//typedef void(^HDKParseObjectCompletion)(NSNumber *server_id);
//
//@class CWStatusBarNotification;
//
//@interface HDKParseObject : NSObject
//
//@property(strong, nonatomic) CWStatusBarNotification *notification;
//
//- (void)setUpAPNS:(UIApplication *)application;
//- (void)setUpRegistration:(NSData *)deviceToken;
//- (void)handleIncomingParseObject:(NSDictionary *)aDict withTitle:(NSString *)aTitle withLookup:(NSNumber *)aServerId forApplication:(UIApplication *)application withCompletion:(HDKParseObjectCompletion)completion;
//- (void)subscribeToParseChannelForProject:(NSString *)aProjectAbbreviation andServerId:(NSNumber *)aNumber;
//
//- (void)setupNotificationBanner:(UIColor *)backgroundColor withFont:(UIFont *)font;
//
//@end
