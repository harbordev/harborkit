//
//  HDKParseObject.m
//  Favement
//
//  Created by Joseph Gorecki on 11/2/15.
//  Copyright © 2015 HarborDev. All rights reserved.
//

#import "HDKParseObject.h"
//#import <Parse/Parse.h>

#import <PromiseKit/PromiseKit.h>
#import <PromiseKit/UIAlertView+PromiseKit.h>
#import <ObjectiveSugar/ObjectiveSugar.h>
#import <CWStatusBarNotification/CWStatusBarNotification.h>

#import "HDKUserObject.h"

//@implementation HDKParseObject

//- (void)setUpAPNS:(UIApplication *)application{
//    
//    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
//                                                    UIUserNotificationTypeBadge |
//                                                    UIUserNotificationTypeSound);
//    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
//                                                                             categories:nil];
//    [application registerUserNotificationSettings:settings];
//    [application registerForRemoteNotifications];
//    
//}
//
//- (void)setUpRegistration:(NSData *)deviceToken{
//    
//    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
//    [currentInstallation setDeviceTokenFromData:deviceToken];
//    //currentInstallation.channels = @[ @"global" ];
//    [currentInstallation saveInBackground];
//    
//}
//
//- (void)handleIncomingParseObject:(NSDictionary *)aDict withTitle:(NSString *)aTitle withLookup:(NSNumber *)aServerId forApplication:(UIApplication *)application withCompletion:(HDKParseObjectCompletion)completion{
//    
//    HDKUserObject *aUserObject = [HDKUserObject getUserObjectFromDefaults];
//    
//    //NSLog(@"a push dictionary came in: %@ ... current user id is... %@", aDict, aUserObject.userId);
//    
//    if(![aDict hasKey:@"user_id"]){ //no user data so we're handling this like a generic push.
//        
//        [PFPush handlePush:aDict];
//        
//    }else{
//        
//        if (aUserObject.userId.integerValue != [aDict[@"user_id"] integerValue]){ //its not me.
//            
//            if ([aServerId integerValue]) { //server ID is available
//                
//                if ([application applicationState] == UIApplicationStateActive) {
//                    
//                    NSString *aString = aDict[@"aps"][@"alert"];
//                    
//                    [self.notification displayNotificationWithMessage:aString forDuration:3.0f];
//                    
//                    __weak typeof(self) weakSelf = self;
//                    self.notification.notificationTappedBlock = ^{
//                        
//                        [weakSelf.notification dismissNotificationWithCompletion:^{
//                            
//                            if(completion){
//                                
//                                completion( aServerId );
//                                
//                            }
//                            
//                        }];
//                        
//                    };
//                    
//                }else{
//                    
//                    if(completion){
//                        
//                        completion( aServerId );
//                        
//                    }
//                    
//                }
//                
//            }
//            
//        }else{
//            
//            //NSLog(@"doing nothing");
//            
//        }
//        
//    }
//    
//}
//
//- (void)subscribeToParseChannelForProject:(NSString *)aProjectAbbreviation andServerId:(NSNumber *)aNumber{
//    
//    NSError *pushError;
//    
//    if(![PFPush subscribeToChannel:[NSString stringWithFormat:@"%@%@", aProjectAbbreviation, aNumber] error:&pushError]){
//        
//        //NSLog(@"%s push error %@", __PRETTY_FUNCTION__, pushError.localizedDescription);
//        
//    }
//    
//}
//
//- (void)setupNotificationBanner:(UIColor *)backgroundColor withFont:(UIFont *)font{
//    
//    self.notification = [[CWStatusBarNotification alloc] init];
//    self.notification.notificationLabelBackgroundColor = backgroundColor;
//    self.notification.notificationStyle = CWNotificationStyleNavigationBarNotification;
//    self.notification.notificationAnimationType = CWNotificationAnimationStyleTop;
//    self.notification.notificationLabelFont = font;
//    self.notification.notificationLabel.numberOfLines = 2;
//    self.notification.notificationLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    
//}
//
//@end
