//
//  HDKNibView.m
//  HarborKit
//
//  Created by Joseph Gorecki on 7/16/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKNibView.h"

@implementation HDKNibView

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    
    if (self){
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self.class) owner:self options:0];
        
        UIView *aView = nib.firstObject;
        
        aView.frame = self.bounds;
        
        [self addSubview:aView];
        
    }
    
    return self;
    
}

@end
