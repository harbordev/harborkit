//
//  HDKImageSelectorDataProvider.m
//  HarborKit
//
//  Created by Joseph Gorecki on 8/22/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import "HDKImageSelectorDataProvider.h"
#import <GPUImage/GPUImage.h>

@implementation HDKImageSelectorDataProvider

+ (NSArray *)arrayOfImageFilters{

    HDKImageSelectorDataProvider *provider = [HDKImageSelectorDataProvider new];

    return @[@"No Filter",
             [provider createPixellatedFilter],
             [provider createSepiaFilter],
             [provider createHazeColorFilter],
             [provider createUnSharpMaskFilter],
             [provider createColorInvertFilter],
             [provider createGrayscaleFilter],
             [provider createSketchFilter],
             [provider createVignetteFilter],
             [provider createToonFilter],
             [provider createTiltShiftFilter],
             [provider createBlurFilter],
             ];

}

- (GPUImageOutput<GPUImageInput> *)createPixellatedFilter{

    GPUImagePixellateFilter *filter = [[GPUImagePixellateFilter alloc] init];
    
    [filter setFractionalWidthOfAPixel:0.03];
    
    return filter;
    
}

- (GPUImageOutput<GPUImageInput> *)createSepiaFilter{
    
    GPUImageSepiaFilter *filter = [[GPUImageSepiaFilter alloc] init];
    
    [filter setIntensity:0.7];
    
    return filter;
    
}

- (GPUImageOutput<GPUImageInput> *)createHazeColorFilter{

    GPUImageHazeFilter *filter = [[GPUImageHazeFilter alloc] init];
    
    [filter setSlope:-.3];
    [filter setDistance:-.3];
    
    return filter;

}

- (GPUImageOutput<GPUImageInput> *)createUnSharpMaskFilter{

    GPUImageUnsharpMaskFilter *filter = [[GPUImageUnsharpMaskFilter alloc] init];
    
    filter.intensity = 6.0;
    
    return filter;

}

- (GPUImageOutput<GPUImageInput> *)createColorInvertFilter{

    GPUImageColorInvertFilter *filter = [[GPUImageColorInvertFilter alloc] init];
    
    return filter;

}

- (GPUImageOutput<GPUImageInput> *)createGrayscaleFilter{

    GPUImageGrayscaleFilter *filter = [[GPUImageGrayscaleFilter alloc] init];
    
    return filter;

}

- (GPUImageOutput<GPUImageInput> *)createSketchFilter{
    
    GPUImageSketchFilter *filter = [[GPUImageSketchFilter alloc] init];
    
    return filter;
    
}

- (GPUImageOutput<GPUImageInput> *)createVignetteFilter{
    
    GPUImageVignetteFilter *filter = [[GPUImageVignetteFilter alloc] init];
    
    return filter;
    
}

- (GPUImageOutput<GPUImageInput> *)createToonFilter{
    
    GPUImageToonFilter *filter = [[GPUImageToonFilter alloc] init];
    
    return filter;
    
}

- (GPUImageOutput<GPUImageInput> *)createTiltShiftFilter{
    
    GPUImageTiltShiftFilter *filter = [[GPUImageTiltShiftFilter alloc] init];
    
    return filter;
    
}

- (GPUImageOutput<GPUImageInput> *)createBlurFilter{
    
    GPUImageiOSBlurFilter *filter = [[GPUImageiOSBlurFilter alloc] init];
    
    filter.blurRadiusInPixels = 3.0;
    filter.saturation = 0.3;
    
    return filter;
    
}

@end
