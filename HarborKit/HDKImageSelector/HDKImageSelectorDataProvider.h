//
//  HDKImageSelectorDataProvider.h
//  HarborKit
//
//  Created by Joseph Gorecki on 8/22/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HDKImageSelectorDataProvider : NSObject

+ (NSArray *)arrayOfImageFilters;

@end
