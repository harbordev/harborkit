//
//  HDKImageSelectorViewController.h
//  HarborKit
//
//  Created by Joseph Gorecki on 8/20/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import "HDKBaseViewController.h"
#import "HDKGridBackedViewController.h"

typedef void(^HDKImageSelectorViewControllerCompletion)(void);

@protocol HDKImageSelectorViewControllerDelegate <NSObject>

- (void)didFinishPickingImage:(UIImage *)anImage withMovieURL:(NSString *)aURLString;

@end

@interface HDKImageSelectorViewController : HDKGridBackedViewController

@property(nonatomic, weak) id<HDKImageSelectorViewControllerDelegate>delegate;

+ (HDKImageSelectorViewController *)createImageSelectionControllerWithViewController:(UIViewController *)aViewController andDelegate:(id<HDKImageSelectorViewControllerDelegate>)aDelegate;

- (void)showActionSheet;
- (void)imageFromLibrary;
- (void)imageFromCamera;

@end


