//
//  HDKImageSelectorViewController.m
//  HarborKit
//
//  Created by Joseph Gorecki on 8/20/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import "HDKImageSelectorViewController.h"
#import "HDKS3VideoManager.h"
#import "HDKImageSelectorDataProvider.h"
#import "HDKDataSourceable.h"

#import "UIViewController+containers.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import <libextobjc/EXTScope.h>
#import <GPUImage/GPUImage.h>

@interface HDKImageSelectorViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, HDKDataSourceable>

@property (strong, nonatomic) UIImage *selectedImage;
@property (strong, nonatomic) NSString *videoFilePath;

@property (strong, nonatomic) UIViewController *viewController;

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

@property (readwrite) BOOL newMedia;
@property (readwrite) BOOL saving;

@end

@implementation HDKImageSelectorViewController

+ (HDKImageSelectorViewController *)createImageSelectionControllerWithViewController:(UIViewController *)aViewController andDelegate:(id<HDKImageSelectorViewControllerDelegate>)aDelegate{
    
    HDKImageSelectorViewController *imageSelectorViewController = [[HDKImageSelectorViewController alloc] init];
    
    imageSelectorViewController.delegate = aDelegate;
    imageSelectorViewController.viewController = aViewController;
    
    imageSelectorViewController.view.frame = CGRectMake(0, 0, CGRectGetWidth(aViewController.view.frame), CGRectGetHeight(aViewController.view.frame) + 60);
    imageSelectorViewController.view.alpha = 0.0;
    
    return imageSelectorViewController;
    
}

- (void)showActionSheet{
    
    [self.viewController containerAddChildViewController:self];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *pickPhoto = [UIAlertAction actionWithTitle:@"Choose from Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self imageFromLibrary];
        
    }];
    
    UIAlertAction *takePhoto = [UIAlertAction actionWithTitle:@"Use Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self imageFromCamera];
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        //[self.viewController containerRemoveChildViewController:self];
        
    }];
    
    [alert addAction:pickPhoto];
    [alert addAction:takePhoto];
    [alert addAction:cancel];
    
    [self.viewController presentViewController:alert animated:YES completion:nil];
    
}

- (void)viewDidLoad {
    
    self.arr = @[];
    
    self.needsServerRefresh = NO;
    
    [self configureWithDataSource:[HDKBaseGridDataSource class] andArray:self.arr registeredCellClass:[UICollectionViewCell class] cellBlock:^(UICollectionViewCell *cell, id object) {
        
        UIImageView *anImageView = [[UIImageView alloc] initWithFrame:cell.bounds];
        
        anImageView.contentMode = UIViewContentModeScaleAspectFill;
        anImageView.clipsToBounds = YES;
        
        if([object isKindOfClass:[GPUImageOutput class]]){
            
            GPUImageOutput *filter = object;
            
            UIImage *filteredImage = [filter imageByFilteringImage:self.selectedImage];
            
            anImageView.image = filteredImage;
            
        }else{
            
            anImageView.image = self.selectedImage;
            
        }
        
        [cell.contentView addSubview:anImageView];
        
        
    }];
    
    [super viewDidLoad];
    
    self.title = @"Filters";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveButtonWasPressed:)];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

- (void)dealloc{
    
    self.delegate = nil;
    self.viewController = nil;
    _delegate = nil;
    
}

#pragma mark - imagePicker
- (void)imageFromCamera{
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage, (NSString *) kUTTypeMovie];
        
        imagePicker.allowsEditing = YES;
        
        [self.viewController presentViewController:imagePicker animated:YES completion:nil];
        
        _newMedia = YES;
        
    }
    
}

- (void)imageFromLibrary{
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage, (NSString *) kUTTypeMovie];
        
        imagePicker.allowsEditing = YES;
        
        [self.viewController presentViewController:imagePicker animated:YES completion:nil];
        
        _newMedia = NO;
        
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        
        UIImage *image = info[UIImagePickerControllerEditedImage];
        
        self.selectedImage = image;
        
        if (_newMedia){
            
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
            
        }
        
    } else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) {
        
        self.selectedImage = [[HDKS3VideoManager sharedInstance] generateImage:(NSString *)[[info objectForKey:UIImagePickerControllerMediaURL] path]];
        self.videoFilePath = (NSString *)[[info objectForKey:UIImagePickerControllerMediaURL] path];
        
    }
    
    self.mainImageView.image = self.selectedImage;
    
    self.arr = [HDKImageSelectorDataProvider arrayOfImageFilters];
    
    [self refresh];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        self.view.alpha = 1.0;
        
        [self.viewController.navigationController pushViewController:self animated:YES];
        
    }];
    
}

#pragma mark - HDKDataSourceable
- (void)didSelectRowWithObject:(id)anObject andCompletion:(HDKCompletionBlock)completion{
    
    if([anObject isKindOfClass:[GPUImageOutput class]]){
        
        GPUImageOutput *filter = anObject;
        
        self.mainImageView.image = [filter imageByFilteringImage:self.selectedImage];
        
    }else{
        
        self.mainImageView.image = self.selectedImage;
        
    }
    
}

- (IBAction)saveButtonWasPressed:(id)sender {
    
    if(_saving == YES){
        
        return;
        
    }
    
    _saving = YES;
    
    [self.delegate didFinishPickingImage:self.mainImageView.image withMovieURL:self.videoFilePath];
    
}

@end
