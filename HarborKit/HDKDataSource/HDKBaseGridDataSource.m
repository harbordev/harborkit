//
//  HDKBaseGridDataSource.m
//  HDK
//
//  Created by Joseph Gorecki on 4/28/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKBaseGridDataSource.h"

@implementation HDKBaseGridDataSource

- (instancetype)initWithCollectionView:(UICollectionView *)aCollectionView cellBlock:(HDKGridCellBlock)aCellBlock array:(NSArray *)anArray delegate:(id<HDKDataSourceable>)aDelegate{
    
    self = [super init];
    
    if(self){
        
        _delegate = aDelegate;
        
        _collectionView = aCollectionView;
        
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        
        _collectionView.emptyDataSetDelegate = self;
        _collectionView.emptyDataSetSource = self;
        
        _cellBlock = [aCellBlock copy];
        _arr = anArray;
        
    }
    
    return self;
    
}

- (void)dealloc{
    
    _delegate = nil;
    _collectionView.delegate = nil;
    _collectionView.dataSource = nil;
    _collectionView.emptyDataSetSource = nil;
    _collectionView.emptyDataSetDelegate = nil;
    
}

- (void)reloadData:(HDKCompletionBlock)completion{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.collectionView reloadData];
        
        if(completion){
            
            completion();
            
        }
        
    });
    
}

- (void)relayoutVisibleCells{
    
    NSArray *visibleCells = self.collectionView.visibleCells;
    
    for (UITableView *cell in visibleCells) {
        
        [cell setNeedsDisplay];
        
    }
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.arr.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kHDKCellReuseIdentifier forIndexPath:indexPath];
    
    self.cellBlock(cell, self.arr[indexPath.row]);
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if([self.delegate respondsToSelector:@selector(didSelectRowWithObject:andCompletion:)]){
        
        id object = self.arr[indexPath.row];
        
        [self.delegate didSelectRowWithObject:object andCompletion:nil];
        
    }
    
}

#pragma mark - handle empty state
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    
    NSString *text = @"No data found.";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:12.0f],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
    
}

@end
