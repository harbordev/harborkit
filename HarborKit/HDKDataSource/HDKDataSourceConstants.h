//
//  HDKBaseDataSource.h
//  HDK
//
//  Created by Joseph Gorecki on 3/11/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^HDKCellBlock)(UITableViewCell *cell, id object);
typedef void(^HDKGridCellBlock)(UICollectionViewCell *cell, id object);
typedef void(^HDKCompletionBlock)(void);
typedef void(^HDKCreatedCompletionBlock)(NSNumber *aServerId);
typedef void(^HDKUpdatedCompletionBlock)(id object);

extern NSString * const kHDKCellReuseIdentifier;
extern NSString * const kHDKHeaderReuseIdentifier;