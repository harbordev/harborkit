#import <Foundation/Foundation.h>

NSString * const kHDKCellReuseIdentifier = @"Cell";
NSString * const kHDKHeaderReuseIdentifier = @"Header";