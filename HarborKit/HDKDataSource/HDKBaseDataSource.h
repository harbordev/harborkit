//
//  HDKBaseDataSource.h
//  HDK
//
//  Created by Joseph Gorecki on 3/11/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HDKDataSourceable.h"
#import "HDKDataSourceConstants.h"

#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface HDKBaseDataSource : NSObject<UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property (copy, nonatomic) NSArray *arr;
@property (weak, nonatomic) id<HDKDataSourceable>delegate;
@property (copy, nonatomic) HDKCellBlock cellBlock;

@property (strong, nonatomic) id relatedObject;

@property (strong, nonatomic) UITableView *tableView;

@property (readwrite) NSInteger offset;
@property (readwrite) NSInteger limit;

@property (readwrite) BOOL editable;

- (instancetype)initWithTableView:(UITableView *)aTableView
                        cellBlock:(HDKCellBlock)aCellBlock
                            array:(NSArray *)anArray
                         delegate:(id<HDKDataSourceable>)aDelegate;

- (void)reloadData:(HDKCompletionBlock)completion;
- (void)reloadDataWithSelections:(NSSet *)selections andCompletion:(HDKCompletionBlock)completion;

- (void)tableCellInternalButtonClick:(id)sender;

@end
