//
//  HDKBaseGridDataSource.h
//  HDK
//
//  Created by Joseph Gorecki on 4/28/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HDKDataSourceable.h"
#import "HDKDataSourceConstants.h"

#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface HDKBaseGridDataSource : NSObject<UICollectionViewDataSource, UICollectionViewDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>

@property (copy, nonatomic) NSArray *arr;
@property (weak, nonatomic) id<HDKDataSourceable>delegate;
@property (copy, nonatomic) HDKGridCellBlock cellBlock;

@property (strong, nonatomic) UICollectionView *collectionView;

- (instancetype)initWithCollectionView:(UICollectionView *)aCollectionView
                             cellBlock:(HDKGridCellBlock)aCellBlock
                                 array:(NSArray *)anArray
                              delegate:(id<HDKDataSourceable>)aDelegate;

- (void)reloadData:(HDKCompletionBlock)completion;

@end
