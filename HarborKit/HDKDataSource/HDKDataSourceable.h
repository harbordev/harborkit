//
//  HDKDatasourceable.h
//  HDK
//
//  Created by Joseph Gorecki on 4/14/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HDKDataSourceConstants.h"

@protocol HDKDataSourceable <NSObject>

@optional

- (void)didSelectRowWithObject:(id)anObject andCompletion:(HDKCompletionBlock)completion;
- (void)didReachLastRowAndNeedsMoreDataFromController;
- (void)didSelectButtonWithObject:(id)anObject andButton:(UIButton *)aButton andCompletion:(HDKCompletionBlock)completion;
- (void)didSelectRowWithObject:(id)anObject atIndexPath:(NSIndexPath *)anIndexPath andCompletion:(HDKCompletionBlock)completion;
- (void)didSelectHeaderButton:(UIButton *)aButton andCompletion:(HDKCompletionBlock)completion;
- (void)didSelectRowToDeleteWithObject:(id)anObject atIndexPath:(NSIndexPath *)anIndexPath withCompletion:(HDKCompletionBlock)completion;

@end
