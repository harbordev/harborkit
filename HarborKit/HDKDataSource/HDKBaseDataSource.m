//
//  HDKBaseDataSource.m
//  HDK
//
//  Created by Joseph Gorecki on 3/11/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKBaseDataSource.h"

@interface HDKBaseDataSource ()

@property (strong, nonatomic) NSSet *selections;

@end

@implementation HDKBaseDataSource

- (instancetype)initWithTableView:(UITableView *)aTableView cellBlock:(HDKCellBlock)aCellBlock array:(NSArray *)anArray delegate:(id<HDKDataSourceable>)aDelegate{
    
    self = [super init];
    
    if(self){
        
        _delegate = aDelegate;
        
        _tableView = aTableView;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        
        _tableView.emptyDataSetSource = self;
        _tableView.emptyDataSetDelegate = self;
        
        _cellBlock = [aCellBlock copy];
        _arr = anArray;
        
        _limit = 20;
        
    }
    
    return self;
    
}

- (void)dealloc{
    
    _delegate = nil;
    _tableView.delegate = nil;
    _tableView.dataSource = nil;
    _tableView.emptyDataSetDelegate = nil;
    _tableView.emptyDataSetSource = nil;
    
}

- (void)reloadData:(HDKCompletionBlock)completion{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.tableView reloadData];
        
        if(completion){
            
            completion();
            
        }
        
    });
    
}

- (void)reloadDataWithSelections:(NSSet *)selections andCompletion:(HDKCompletionBlock)completion{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.selections = selections;
        
        [self.tableView reloadData];
        
        if(completion){
            
            completion();
            
        }
        
    });
    
}

- (CGFloat)tableView:(UITableViewCell *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 140.0;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.arr.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kHDKCellReuseIdentifier];
    
    self.cellBlock(cell, self.arr[indexPath.row]);
    
    if( [self.selections containsObject:self.arr[indexPath.row]] ){
        
        [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        
    }else{
        
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
        
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if([self.delegate respondsToSelector:@selector(didSelectRowWithObject:andCompletion:)]){
        
        id object = self.arr[indexPath.row];
        
        [self.delegate didSelectRowWithObject:object andCompletion:nil];
        
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger lastSectionIndex = [tableView numberOfSections] - 1;
    NSInteger lastRowIndex = [tableView numberOfRowsInSection:lastSectionIndex] - 1;
    
    if ((indexPath.section == lastSectionIndex) && (indexPath.row == lastRowIndex) && (self.arr.count >= self.limit)) {
        
        if([self.delegate respondsToSelector:@selector(didReachLastRowAndNeedsMoreDataFromController)]){
            
            [self.delegate didReachLastRowAndNeedsMoreDataFromController];
            
        }
        
    }
}

#pragma mark - Swipe to edit
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(!self.editable){
        
        return UITableViewCellEditingStyleNone;
        
    }
    
    return UITableViewCellEditingStyleDelete;
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.delegate didSelectRowToDeleteWithObject:[self.arr objectAtIndex:indexPath.row] atIndexPath:indexPath withCompletion:nil];
        
    }
}


#pragma mark - TableCellButtons
- (void)tableCellInternalButtonClick:(id)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    if([self.delegate respondsToSelector:@selector(didSelectButtonWithObject:andButton:andCompletion:)]){
        
        id object = self.arr[indexPath.row];
        
        [self.delegate didSelectButtonWithObject:object andButton:sender andCompletion:nil];
        
    }
    
}

#pragma mark - handle empty state
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView{
    
    NSString *text = @"No data found.";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:12.0f],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return [[UIView alloc] initWithFrame:CGRectZero];
    
}

@end
