//
//  HDKBaseViewController.h
//  HarborKit
//
//  Created by Joseph Gorecki on 8/13/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Generic Completion Block
 */
typedef void(^HDKBaseViewControllerCompletionBlock)(void);

/**
 *  Base View Controller inherits from UIViewController
 */
@interface HDKBaseViewController : UIViewController

/**
*  If YES then left bar button is set with a CANCEL & right is a DONE
*/
@property (readwrite, nonatomic) BOOL isBeingPresented;

/**
 *  Helper method to make a navigation controller
 *
 *  @param aViewController viewcontroller embedded in the nav controller
 *
 *  @return navigation controller
 */
- (UINavigationController *)createNavControllerForViewController:(UIViewController *)aViewController;

/**
 *  Handles a routing request with an optional alertview (if text is not blank)
 *
 *  @param alertMessage Alert message to show if set
 *  @param completion   Completion block
 */
- (void)handleRoutingRequest:(NSString *)alertMessage withCompletion:(HDKBaseViewControllerCompletionBlock)completion;

/**
 *  Present on the top key window if not currently the top controller
 */
- (void)presentOnTopOfAllOtherControllers DEPRECATED_MSG_ATTRIBUTE("Doesn't work as intended.");;

/**
 *  Closes presented controllers
 */
- (void)closeTopPresentedController:(id)sender DEPRECATED_MSG_ATTRIBUTE("Use dismissViewController");;

@end
