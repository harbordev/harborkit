//
//  HDKBaseViewController.m
//  HarborKit
//
//  Created by Joseph Gorecki on 8/13/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKBaseViewController.h"
#import <PromiseKit/PromiseKit.h>
#import <PromiseKit/UIAlertView+PromiseKit.h>

@interface HDKBaseViewController ()

@end

@implementation HDKBaseViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"<<" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    if (self.isBeingPresented) {
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(closeTopPresentedController:)];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closeTopPresentedController:)];
        
    }
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
    }
    
}

- (void)didReceiveMemoryWarning {
 
    [super didReceiveMemoryWarning];
    
}

- (UINavigationController *)createNavControllerForViewController:(UIViewController *)aViewController;{
    
    NSParameterAssert(aViewController);
    
    UINavigationController *aNavController = [[UINavigationController alloc] initWithRootViewController:aViewController];
    
    return aNavController;
}

- (void)presentOnTopOfAllOtherControllers{ //deprecated for bad results
    
    dispatch_async(dispatch_get_main_queue(), ^{

        id rootViewController = [[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];
        
        [rootViewController presentViewController:[self createNavControllerForViewController:self] animated:YES completion:nil];

    });
    
}

- (void)closeTopPresentedController:(id)sender{ //deprecated
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)handleRoutingRequest:(NSString *)alertMessage withCompletion:(HDKBaseViewControllerCompletionBlock)completion{
    
    if(alertMessage.length > 0){
        
        UIAlertView *anAlertView = [[UIAlertView alloc] initWithTitle:nil message:alertMessage delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Okay", nil];
        
        [anAlertView promise].then(^(NSNumber *aButton){
            
            switch (aButton.integerValue) {
                    
                case 1:{
                    
                    if(completion){
                        
                        completion();
                    
                    }
                    
                }break;
                    
                default:
                    
                    //no op
                    
                    break;
                    
            }
            
            
        });
        
    }else{
        
        if(completion){
            completion();
        }
    }
    
}

@end
