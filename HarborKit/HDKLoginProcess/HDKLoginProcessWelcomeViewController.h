//
//  HDKLoginProcessWelcomeViewController.h
//  HarborKit
//
//  Created by Joseph Gorecki on 11/23/15.
//  Copyright © 2015 HarborDev. All rights reserved.
//

#import "HDKBaseViewController.h"
#import "HDKLoginable.h"

@interface HDKLoginProcessWelcomeViewController : HDKBaseViewController

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIButton *signupButton;
@property (strong, nonatomic) IBOutlet UIButton *forgotButton;
@property (strong, nonatomic) IBOutlet UILabel *copyrightLabel;

@property (nonatomic, copy) NSString *loginPath;
@property (nonatomic, copy) NSString *signupPath;
@property (nonatomic, copy) NSString *createPath;

@property (nonatomic, copy) NSString *welcomeNib;
@property (nonatomic, copy) NSString *loginNib;
@property (nonatomic, copy) NSString *signupNib;
@property (nonatomic, copy) NSString *createNib;

@property (strong, nonatomic) id<HDKLoginable>delegate;

@end
