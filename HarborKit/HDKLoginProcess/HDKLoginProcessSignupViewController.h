//
//  HDKLoginProcessSignupViewController.h
//  HarborKit
//
//  Created by Joseph Gorecki on 11/23/15.
//  Copyright © 2015 HarborDev. All rights reserved.
//

#import "HDKBaseViewController.h"

@protocol HDKLoginProcessSignupViewControllerDelegate <NSObject>

- (void)didFinishSignupWithResult:(BOOL)aResult;

@end

@interface HDKLoginProcessSignupViewController : HDKBaseViewController

@property (copy, nonatomic) NSString *signupPath;

@property (weak, nonatomic) id<HDKLoginProcessSignupViewControllerDelegate>delegate;

@end
