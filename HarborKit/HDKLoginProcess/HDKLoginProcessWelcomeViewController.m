//
//  HDKLoginProcessWelcomeViewController.m
//  HarborKit
//
//  Created by Joseph Gorecki on 11/23/15.
//  Copyright © 2015 HarborDev. All rights reserved.
//

#import "HDKLoginProcessWelcomeViewController.h"
#import "HDKLoginProcessSignupViewController.h"
#import "HDKLoginProcessLoginViewController.h"

@interface HDKLoginProcessWelcomeViewController ()<HDKLoginProcessLoginViewControllerDelegate, HDKLoginProcessSignupViewControllerDelegate>

@end

@implementation HDKLoginProcessWelcomeViewController

- (void)viewDidLoad {

    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];

}

#pragma mark - UIButton (IBAction)
- (IBAction)loginButtonWasPressed:(id)sender {

    HDKLoginProcessLoginViewController *loginViewController = [[HDKLoginProcessLoginViewController alloc] initWithNibName:self.loginNib bundle:nil];
    
    loginViewController.isBeingPresented = YES;
    
    loginViewController.loginPath = self.loginPath;
    
    loginViewController.delegate = self;
    
    [self presentViewController:[self createNavControllerForViewController:loginViewController] animated:YES completion:nil];
    
}

- (IBAction)signupButtonWasPressed:(id)sender {
    
    HDKLoginProcessSignupViewController *signupViewController = [[HDKLoginProcessSignupViewController alloc] initWithNibName:self.signupNib bundle:nil];
    
    signupViewController.isBeingPresented = YES;
    
    signupViewController.signupPath = self.signupPath;
    
    signupViewController.delegate = self;
    
    [self presentViewController:[self createNavControllerForViewController:signupViewController] animated:YES completion:nil];
    
}

- (IBAction)forgotButtonWasPressed:(id)sender {
    
    //NSLog(@"forgot was pressed");
    
}

#pragma mark - delegates
- (void)didFinishLoginWithResult:(BOOL)aResult{

    if (aResult){
    
        //NSLog(@"finished the login successfully");;
        [self.delegate didLoginSucessfullyWithLoginProcess:nil];
    
    }else{
    
        [self.delegate didFailToLoginWithLoginProcess:nil];
    
    }

}

- (void)didFinishSignupWithResult:(BOOL)aResult{

    if (aResult){
        
        //NSLog(@"finished the signup successfully");;
        [self.delegate didLoginSucessfullyWithLoginProcess:nil];
        
    }else{
        
        [self.delegate didFailToLoginWithLoginProcess:nil];
        
    }

}

@end
