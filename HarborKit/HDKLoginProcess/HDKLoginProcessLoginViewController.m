//
//  HDKLoginProcessLoginViewController.m
//  HarborKit
//
//  Created by Joseph Gorecki on 11/23/15.
//  Copyright © 2015 HarborDev. All rights reserved.
//

#import "HDKLoginProcessLoginViewController.h"

#import "HDKValidationHelper.h"
#import "HDKUserObject.h"
#import "HDKHudHelper.h"

@interface HDKLoginProcessLoginViewController ()

@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation HDKLoginProcessLoginViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"Login";
    
    self.navigationItem.rightBarButtonItem = nil;
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

#pragma mark - UIBarButtonItem
- (void)close:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)loginButtonWasPressed:(id)sender {
    
    BOOL isValidUserName = [HDKValidationHelper isValidEntry:self.usernameTextField.text];
    BOOL isValidPassword = [HDKValidationHelper isValidPasswordSimple:self.passwordTextField.text];
    
    if(isValidUserName && isValidPassword){
        
        [self loginToAccountOnServerWithParameters:@{@"username": self.usernameTextField.text, @"password": self.passwordTextField.text} withMessage:@"Logging in to Application"];
        
    }else{
        
        [HDKHudHelper showErrorWithStatus:@"Sorry, please fill out both fields fully."];
        
    }
    
}

#pragma mark - Submit to server
- (void)loginToAccountOnServerWithParameters:(NSDictionary *)parameters withMessage:(NSString *)aMessage{
    
    [HDKHudHelper showStatusAndMask:aMessage];
    
    [HDKUserObject syncUserObjectWithServer:self.loginPath withParameters:parameters completion:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self dismissViewControllerAnimated:YES completion:^{
                
                [self.delegate didFinishLoginWithResult:YES];
                
            }];
            
        });
        
        [HDKHudHelper dismiss];
        
    } error:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [HDKHudHelper showErrorWithStatus:@"Could not login to account on server."];
            
            [self.delegate didFinishLoginWithResult:NO];
            
        });
        
    }];
    
}

@end
