//
//  HDKLoginProcessLoginViewController.h
//  HarborKit
//
//  Created by Joseph Gorecki on 11/23/15.
//  Copyright © 2015 HarborDev. All rights reserved.
//

#import "HDKBaseViewController.h"

@protocol HDKLoginProcessLoginViewControllerDelegate <NSObject>

- (void)didFinishLoginWithResult:(BOOL)aResult;

@end

@interface HDKLoginProcessLoginViewController : HDKBaseViewController

@property(copy, nonatomic) NSString *loginPath;

@property(weak, nonatomic) id<HDKLoginProcessLoginViewControllerDelegate>delegate;

@end
