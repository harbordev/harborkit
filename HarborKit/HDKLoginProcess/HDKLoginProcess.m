//
//  HDKLoginProcess.m
//  HarborKit
//
//  Created by Joseph Gorecki on 8/17/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKLoginProcess.h"
#import "HDKUserObject.h"

#import "HDKLoginProcessWelcomeViewController.h"
#import "UIViewController+containers.h"

@interface HDKLoginProcess ()<HDKLoginable>

@property (strong) UIViewController *rootViewController;
@property (strong) HDKLoginProcessWelcomeViewController *loginProcessViewController;

@end

@implementation HDKLoginProcess

- (UIViewController *)startLoginProcessWithApplication:(UIViewController *)rootViewController{
    
    self.rootViewController = rootViewController;
    
    HDKUserObject *aUserObject = [HDKUserObject getUserObjectFromDefaults];
    
    if ( aUserObject == nil ) {
        
        //NSLog(@"user doesn't exist - defaulting to the welcome screen");
        
        self.loginProcessViewController = [[HDKLoginProcessWelcomeViewController alloc] initWithNibName:self.welcomeNib bundle:nil];
        
        self.loginProcessViewController.view.frame = self.rootViewController.view.frame;
        
        self.loginProcessViewController.loginPath = self.loginPath;
        self.loginProcessViewController.signupPath = self.signupPath;
        self.loginProcessViewController.createPath = self.createPath;
        
        self.loginProcessViewController.loginNib = self.loginNib;
        self.loginProcessViewController.signupNib = self.signupNib;
        self.loginProcessViewController.createNib = self.createNib;
        
        self.loginProcessViewController.delegate = self;
        
        [self.rootViewController containerAddChildViewController:self.loginProcessViewController];

    }
    
    return self.rootViewController;

}

- (void)stopLoginProcessWithCompletion:(HDKLoginProcessCompletionBlock)completion{

    //NSLog(@"removing the login process");
    
    [self.rootViewController containerRemoveChildViewController:self.loginProcessViewController];

}

#pragma mark - HDKLoginProcess
- (void)didLoginSucessfullyWithLoginProcess:(HDKLoginProcessCompletionBlock)completion{

    //NSLog(@"login successfully happened");
    
    [self stopLoginProcessWithCompletion:completion];

}

- (void)didFailToLoginWithLoginProcess:(HDKLoginProcessErrorBlock)errorBlock{

    //NSLog(@"failed to login.");

}

#pragma mark - Cleanup
- (void)dealloc{

    self.loginProcessViewController.delegate = nil;

}

@end
