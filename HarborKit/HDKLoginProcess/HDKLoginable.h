//
//  HDKLoginable.h
//  HarborKit
//
//  Created by Joseph Gorecki on 11/23/15.
//  Copyright © 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^HDKLoginProcessCompletionBlock)(void);
typedef void(^HDKLoginProcessErrorBlock)(NSError *anError);

@protocol HDKLoginable <NSObject>

- (void)didLoginSucessfullyWithLoginProcess:(HDKLoginProcessCompletionBlock)completion;
- (void)didFailToLoginWithLoginProcess:(HDKLoginProcessErrorBlock)errorBlock;

@end
