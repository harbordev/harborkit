//
//  HDKLoginProcess.h
//  HarborKit
//
//  Created by Joseph Gorecki on 8/17/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@import UIKit;

#import "HDKLoginable.h"

@interface HDKLoginProcess : NSObject

@property (nonatomic, copy) NSString *loginPath;
@property (nonatomic, copy) NSString *signupPath;
@property (nonatomic, copy) NSString *createPath;

@property (nonatomic, copy) NSString *welcomeNib;
@property (nonatomic, copy) NSString *loginNib;
@property (nonatomic, copy) NSString *signupNib;
@property (nonatomic, copy) NSString *createNib;

- (UIViewController *)startLoginProcessWithApplication:(UIViewController *)rootViewController;

- (void)stopLoginProcessWithCompletion:(HDKLoginProcessCompletionBlock)completion;

@end
