//
//  HDKLoginProcessSignupViewController.m
//  HarborKit
//
//  Created by Joseph Gorecki on 11/23/15.
//  Copyright © 2015 HarborDev. All rights reserved.
//

#import "HDKLoginProcessSignupViewController.h"

@interface HDKLoginProcessSignupViewController ()

@end

@implementation HDKLoginProcessSignupViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = @"Signup";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(close:)];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

#pragma mark - UIBarButtonItem
- (void)close:(id)sender{

    [self dismissViewControllerAnimated:YES completion:nil];

}

@end
