//
//  HDKTextReaderViewController.m
//  HarborKit
//
//  Created by Joseph Gorecki on 9/10/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKTextReaderViewController.h"

@interface HDKTextReaderViewController ()

@property (strong, nonatomic) NSString *textString;
@property (strong, nonatomic) UIColor *color;
@property (strong, nonatomic) UIFont *font;

@end

@implementation HDKTextReaderViewController

- (instancetype)initWithText:(NSString *)aString withFont:(UIFont *)aFont withTextColor:(UIColor *)aColor andTitle:(NSString *)aTitleString{
    
    self = [super init];
    if(self){
        
        self.title = aTitleString;
        _textString = aString;
        _color = aColor;
        _font = aFont;
        
    }
    return self;
    
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if (self.textString.length > 0) {
        
        NSAttributedString *attributed = [[NSAttributedString alloc] initWithString:self.textString attributes:@{NSFontAttributeName:self.font, NSForegroundColorAttributeName:self.color}];
        
        self.textView.attributedText = attributed;
        
    }
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

@end
