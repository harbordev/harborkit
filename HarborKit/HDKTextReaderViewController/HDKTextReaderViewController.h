//
//  HDKTextReaderViewController.h
//  HarborKit
//
//  Created by Joseph Gorecki on 9/10/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKBaseViewController.h"

@interface HDKTextReaderViewController : HDKBaseViewController

- (instancetype)initWithText:(NSString *)aString withFont:(UIFont *)aFont withTextColor:(UIColor *)aColor andTitle:(NSString *)aTitleString;

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end
