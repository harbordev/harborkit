//
//  HDKRoutingSevice.h
//  HarborKit
//
//  Created by Joseph Gorecki on 8/13/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

typedef void(^HDKRoutingServiceCompletionBlock)(id object);

@interface HDKRoutingSevice : NSObject

- (BOOL)handleURIRouting:(NSURL *)aURL forParameter:(NSString *)paramaterName completionBlock:(HDKRoutingServiceCompletionBlock)completion;

@end
