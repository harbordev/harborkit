//
//  HDKRoutingSevice.m
//  HarborKit
//
//  Created by Joseph Gorecki on 8/13/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKRoutingSevice.h"
#import <Bolts/BFURL.h>
#import <ObjectiveSugar/ObjectiveSugar.h>

@interface HDKRoutingSevice ()

@end

@implementation HDKRoutingSevice

- (BOOL)handleURIRouting:(NSURL *)aURL forParameter:(NSString *)paramaterName completionBlock:(HDKRoutingServiceCompletionBlock)completion{

    NSDictionary *parameters = [BFURL URLWithURL:aURL].inputQueryParameters;

    //NSLog(@"parameters: %@", parameters);
    
    if ([parameters hasKey:paramaterName]){
        
        if(completion){
            completion(parameters[paramaterName]);
        }
        
        return YES;
        
    }
    
    return NO;

}

@end
