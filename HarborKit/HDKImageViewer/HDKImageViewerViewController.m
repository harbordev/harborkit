//
//  HDKImageViewerViewController.m
//  HarborKit
//
//  Created by Joseph Gorecki on 9/23/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import "HDKImageViewerViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface HDKImageViewerViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipe;

@property (strong, nonatomic) IBOutlet UILabel *loadingLabel;

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;

@end

@implementation HDKImageViewerViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.title = self.imageTitle;
    
    //self.avatarImageView.hidden = YES;
    //self.usernameLabel.hidden = YES;
    
    if(self.avatar != nil){
        
        self.avatarImageView.hidden = NO;
        [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:self.avatar] placeholderImage:[UIImage imageNamed:@"fpo_icon"]];
        
    }
    
    if(self.username.length > 0){
        
        self.usernameLabel.hidden = NO;
        self.usernameLabel.text = self.username.capitalizedString;
        
    }
    
    if( self.image ){
        
        self.imageView.image = self.image;
        self.loadingLabel.hidden = YES;
        
    }else{
        
        NSURL *aUrl = [NSURL URLWithString:self.imageURLString];
        [self loadImageFromUrl:aUrl];
        
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closeTopPresentedController:)];
    
}

- (void)loadImageFromUrl:(NSURL *)aUrl{
    
    [[SDWebImageManager sharedManager] loadImageWithURL:aUrl options:SDWebImageContinueInBackground|SDWebImageDownloaderProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
        
        float expected = expectedSize;
        float recieved = receivedSize;
        
        int total = recieved/expected * 100.0;
        
        self.loadingLabel.text = [NSString stringWithFormat:@"%@%@", @(total), @"%"];
        
    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
        if (error) {
            
            //NSLog(@"could not download the image: ");
            self.loadingLabel.text = @"Couldn't download the image.";
            
        }else{
            
            if(image){
                
                self.loadingLabel.hidden = YES;
                self.imageView.image = image;
                
            }
            
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

- (IBAction)swipeToClose:(id)sender {
    
    [self closeTopPresentedController:nil];
    
}

@end
