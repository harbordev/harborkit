//
//  HDKImageViewerObject.m
//  HarborKit
//
//  Created by Joseph Gorecki on 9/23/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import "HDKImageViewerObject.h"

@implementation HDKImageViewerObject

+ (HDKImageViewerObject *)makeObjectForUIImage:(UIImage *)anImage anImageURL:(NSURL *)anImageUrl anAvatarImageURL:(NSURL *)anAvatarImageUrl andUsername:(NSString *)aUsername{

    HDKImageViewerObject *imageViewerObject = [HDKImageViewerObject new];
    
    imageViewerObject.anImage = anImage;
    imageViewerObject.anImageUrl = anImageUrl;
    imageViewerObject.avatarImageUrl = anAvatarImageUrl;
    imageViewerObject.username = aUsername;
    
    return imageViewerObject;

}

@end
