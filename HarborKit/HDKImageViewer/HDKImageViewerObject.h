//
//  HDKImageViewerObject.h
//  HarborKit
//
//  Created by Joseph Gorecki on 9/23/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface HDKImageViewerObject : NSObject

@property (strong, nonatomic) UIImage *anImage;
@property (strong, nonatomic) NSURL *anImageUrl;
@property (strong, nonatomic) NSURL *avatarImageUrl;
@property (strong, nonatomic) NSString *username;

+ (HDKImageViewerObject *)makeObjectForUIImage:(UIImage *)anImage anImageURL:(NSURL *)anImageUrl anAvatarImageURL:(NSURL *)anAvatarImageUrl andUsername:(NSString *)aUsername;

@end
