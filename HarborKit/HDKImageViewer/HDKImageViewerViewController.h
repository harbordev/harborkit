//
//  HDKImageViewerViewController.h
//  HarborKit
//
//  Created by Joseph Gorecki on 9/23/16.
//  Copyright © 2016 HarborDev. All rights reserved.
//

#import "HDKBaseViewController.h"

@interface HDKImageViewerViewController : HDKBaseViewController

/**
 *  Optional image.  If used then image url will not be.
 */
@property (strong, nonatomic) UIImage *image;

/**
 *  Progressive download from webserver.  Continues in background.  Displays percentage downloaded.
 */
@property (copy, nonatomic) NSString *imageURLString;

/**
 *  Title for display in nav bar
 */
@property (copy, nonatomic) NSString *imageTitle;

/**
 *  Array of assets to page through
 */
@property (strong, nonatomic) NSArray *arrayOfAssets;

/**
 *  String for displaying user name of the asset creator
 */
@property (weak, nonatomic) IBOutlet NSString *username;

/**
 *  Avatar Image of asset creator
 */
@property (weak, nonatomic) IBOutlet NSString *avatar;

@end
