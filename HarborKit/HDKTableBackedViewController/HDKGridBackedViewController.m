//
//  HDKGridBackedViewController.m
//  HarborKit
//
//  Created by Joseph Gorecki on 8/24/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKGridBackedViewController.h"
#import "HDKCollectionViewCell.h"
#import "HDKBaseGridDataSource.h"
#import "HDKBaseSyncService.h"
#import "HDKSyncOperationManager.h"

#import <SSPullToRefresh/SSPullToRefresh.h>

@interface HDKGridBackedViewController ()<HDKDataSourceable, SSPullToRefreshViewDelegate, HDKBaseSyncServiceDelegate>

@property (strong, nonatomic) HDKBaseGridDataSource *dataSource;
@property (strong, nonatomic) SSPullToRefreshView *pullToRefreshView;
@property (readwrite) BOOL isLoading;

@end

@implementation HDKGridBackedViewController

- (void)configureWithDataSource:(Class)aDataSource andArray:(NSArray *)anArray registeredCellClass:(Class)aCellViewClass registeredHeaderClass:(Class)aHeaderViewClass cellBlock:(HDKGridCellBlock)cellBlock{
    
    if(aHeaderViewClass != nil){
        
        [self.gridView registerNib:[aHeaderViewClass nib] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kHDKHeaderReuseIdentifier];
        
    }
    
    [self configureWithDataSource:aDataSource andArray:anArray registeredCellClass:aCellViewClass cellBlock:cellBlock];
    
}

- (void)configureWithDataSource:(Class)aDataSource andArray:(NSArray *)anArray registeredCellClass:(Class)aCellViewClass cellBlock:(HDKGridCellBlock)cellBlock{
    
    NSParameterAssert([aDataSource isSubclassOfClass:[HDKBaseGridDataSource class]]);
    NSParameterAssert([anArray isKindOfClass:[NSArray class]]);
    NSParameterAssert(cellBlock);
    
    if( [aCellViewClass isSubclassOfClass:[HDKCollectionViewCell class]] ){
        
        [self.gridView registerNib:[aCellViewClass nib] forCellWithReuseIdentifier:kHDKCellReuseIdentifier];
        
    }else{
        
        [self.gridView registerClass:aCellViewClass forCellWithReuseIdentifier:kHDKCellReuseIdentifier];
        
    }
    
    self.dataSource = [[aDataSource alloc] initWithCollectionView:self.gridView cellBlock:cellBlock array:anArray delegate:self];
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.pullToRefreshView = [[SSPullToRefreshView alloc] initWithScrollView:self.gridView delegate:self];
    
    [self refresh];
    
}

@end
