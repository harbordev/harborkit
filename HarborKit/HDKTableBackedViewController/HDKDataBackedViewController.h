//
//  HDKDataBackedViewController.h
//  HarborKit
//
//  Created by Joseph Gorecki on 9/9/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

@import UIKit;
@import CoreData;

#import "HDKBaseViewController.h"
#import "HDKBaseDataSource.h"
#import "HDKDataSourceConstants.h"
#import "HDKBaseSyncService.h"
#import "HDKSyncOperationManager.h"

#import <SSPullToRefresh/SSPullToRefresh.h>
#import <MagicalRecord/MagicalRecord.h>
#import <libextobjc/extobjc.h>

@protocol HDKDataBackedViewControllerDelegate <NSObject>

- (void)didRefresh;

@end

@interface HDKDataBackedViewController : HDKBaseViewController<HDKDataSourceable, SSPullToRefreshViewDelegate, HDKBaseSyncServiceDelegate>

@property (strong, nonatomic) Class entityClass;
@property (copy, nonatomic) NSString *requestPath;

@property (strong, nonatomic) HDKBaseDataSource *dataSource;

@property (strong, nonatomic) NSDictionary *serverParameters;
@property (strong, nonatomic) NSArray *arrayOfSortDescriptors;
@property (strong, nonatomic) NSPredicate *predicate;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) id<HDKDataBackedViewControllerDelegate>tableDelegate;

@property (strong, nonatomic) id related;

@property (readwrite) BOOL needsServerRefresh;

@property (strong, nonatomic) NSArray *arr;

@property (strong, nonatomic) SSPullToRefreshView *pullToRefreshView;

@property (readwrite) BOOL isLoading;

@end
