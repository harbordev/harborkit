//
//  HDKTableBackedViewController.m
//  HarborKit
//
//  Created by Joseph Gorecki on 8/13/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKTableBackedViewController.h"

#import "HDKBaseSyncService.h"
#import "HDKSyncOperationManager.h"
#import "HDKPaginationManager.h"

#import "HDKTableViewCell.h"

#import "NSManagedObject+HarborKit.h"

#import <SSPullToRefresh/SSPullToRefresh.h>
#import <MagicalRecord/MagicalRecord.h>
#import <libextobjc/extobjc.h>

@interface HDKTableBackedViewController ()<HDKDataSourceable, SSPullToRefreshViewDelegate, HDKBaseSyncServiceDelegate>

@property (strong, nonatomic) SSPullToRefreshView *pullToRefreshView;
@property (readwrite) BOOL isLoading;

@end

@implementation HDKTableBackedViewController

- (void)configureAndCreateTableWithDataSource:(Class)aDataSource andArray:(NSArray *)anArray registeredCellClass:(Class)aCellViewClass cellBlock:(HDKCellBlock)cellBlock{

    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];

    [self.view addSubview:self.tableView];
    
    [self configureWithDataSource:aDataSource andArray:anArray registeredCellClass:aCellViewClass cellBlock:cellBlock];
}

- (void)configureWithDataSource:(Class)aDataSource andArray:(NSArray *)anArray registeredCellClass:(Class)aCellViewClass registeredHeaderClass:(Class)aHeaderViewClass cellBlock:(HDKCellBlock)cellBlock{

    if(aHeaderViewClass != nil){
    
        [self.tableView registerNib:[aHeaderViewClass nib] forHeaderFooterViewReuseIdentifier:kHDKHeaderReuseIdentifier];
        
    }
    
    [self configureWithDataSource:aDataSource andArray:anArray registeredCellClass:aCellViewClass cellBlock:cellBlock];

}

- (void)configureWithDataSource:(Class)aDataSource andArray:(NSArray *)anArray registeredCellClass:(Class)aCellViewClass cellBlock:(HDKCellBlock)cellBlock{
    
    NSParameterAssert([aDataSource isSubclassOfClass:[HDKBaseDataSource class]]);
    NSParameterAssert([anArray isKindOfClass:[NSArray class]]);
    NSParameterAssert(cellBlock);
    
    if( [aCellViewClass isSubclassOfClass:[HDKTableViewCell class]] ){
        
        [self.tableView registerNib:[aCellViewClass nib] forCellReuseIdentifier:kHDKCellReuseIdentifier];
    
    }else{
    
        [self.tableView registerClass:aCellViewClass forCellReuseIdentifier:kHDKCellReuseIdentifier];
        
    }
    
    self.dataSource = [[aDataSource alloc] initWithTableView:self.tableView cellBlock:cellBlock array:anArray delegate:self];
    
    self.dataSource.relatedObject = self.related;
    self.dataSource.editable = self.editable;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.pullToRefreshView = [[SSPullToRefreshView alloc] initWithScrollView:self.tableView delegate:self];
    
    [self loadData];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
 
}

- (void)dealloc{

    _pullToRefreshView.delegate = nil;

}

#pragma mark - #pragma mark - SSPullToRefresh
- (void)refresh {
    
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [self.pullToRefreshView startLoading];
    
    [self loadData];
    
    [self.pullToRefreshView finishLoading];
}

- (void)pullToRefreshViewDidStartLoading:(SSPullToRefreshView *)view {
    
    [self refresh];
    
}

#pragma mark - Data Load from Local or Server
- (void)loadData{
    
    if(self.arr.count > 0){////NSLog(@"-- using local array");
        
        self.dataSource.arr = self.arr;
        
        [self.dataSource reloadData:nil];
        
        if([self.tableDelegate respondsToSelector:@selector(didRefresh)]){
            
            [self.tableDelegate didRefresh];
            
        }
        
    }else{//using model & server
        
        if(self.isLoading){
        
            return;
        
        }
        
        self.isLoading = YES;
        
        if(self.needsServerRefresh){
            
            [self loadDataFromServer];
            
        }else{
            
            [self loadDataFromLocalDataStore];
            
        }
        
        self.isLoading = NO;
        
        if([self.tableDelegate respondsToSelector:@selector(didRefresh)]){
        
            [self.tableDelegate didRefresh];
        
        }
        
    }
    
}

- (void)loadDataFromLocalDataStore{
    
    NSArray *unsortedArray = [[self entityClass] MR_findAllWithPredicate:self.predicate];
    
    NSArray *anArray = nil;
    
    if(self.arrayOfSortDescriptors.count == 0){
        
        anArray = unsortedArray;
        
    }else{
        
        anArray = [unsortedArray sortedArrayUsingDescriptors:self.arrayOfSortDescriptors];
        
    }
    
    self.dataSource.arr = anArray;
    
    [self.dataSource reloadData:nil];
    
}

- (void)loadDataFromServer{
    
    NSParameterAssert(self.serverParameters);
    
    @weakify(self);
    
    HDKBaseSyncService *syncService = [HDKBaseSyncService new];
    
    [syncService getObjects:[self entityClass] fromServerWithDict:self.serverParameters path:self.requestPath withRelated:self.related andCompletion:^{
        
        @strongify(self);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            @strongify(self);
            
            [self loadDataFromLocalDataStore];
            
        });
        
    }andError:nil];

}

- (void)moveToTableTopAfterDelay:(NSTimeInterval)seconds{

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height) animated:YES];
        
    });

}

- (void)moveToTableBottomAfterDelay:(NSTimeInterval)seconds{

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
        [self.tableView scrollRectToVisible:CGRectMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height, self.tableView.bounds.size.width, self.tableView.bounds.size.height) animated:YES];
    
    });
    
}

#pragma mark - HDKDataSourceable
- (void)didSelectRowWithObject:(id)anObject andCompletion:(HDKCompletionBlock)completion{
    
    assert(@"subclass only");
    
}

- (void)didReachLastRowAndNeedsMoreDataFromController{

    NSDictionary *metaDictionary = [[HDKPaginationManager sharedInstance] metaDictionaryForName:NSStringFromClass(self.entityClass)];
    
    ////NSLog(@"last cell has been reached - load more? %@", metaDictionary);
    
    if( ([[HDKPaginationManager sharedInstance] shouldLoadNextSet:metaDictionary])){
        
        NSInteger newOffset = self.offset + self.limit;
        
        self.offset = newOffset;
        
        self.serverParameters = [[HDKPaginationManager sharedInstance] createPaginationDictionary:self.serverParameters offset:newOffset limit:self.limit];
        
        [self refresh];
        
    }else{
        
        ////NSLog(@"there's no more data to load");
        
    }

}

#pragma mark - HDKBaseSyncServiceDelegate
- (void)syncServiceDidComplete{

    //NSLog(@"- sync completed");

}

@end
