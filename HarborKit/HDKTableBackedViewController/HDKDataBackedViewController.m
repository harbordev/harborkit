//
//  HDKDataBackedViewController.m
//  HarborKit
//
//  Created by Joseph Gorecki on 9/9/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKDataBackedViewController.h"

@interface HDKDataBackedViewController ()

@end

@implementation HDKDataBackedViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    
}

- (void)dealloc{
    
    _pullToRefreshView.delegate = nil;
    
}

#pragma mark - #pragma mark - SSPullToRefresh
- (void)refresh {
    
    [self.pullToRefreshView startLoading];
    
    [self loadData];
    
    [self.pullToRefreshView finishLoading];
}

- (void)pullToRefreshViewDidStartLoading:(SSPullToRefreshView *)view {
    
    [self refresh];
    
}

#pragma mark - Data Load from Local or Server
- (void)loadData{
    
    if(self.arr.count > 0){//NSLog(@"-- using local array");
        
        self.dataSource.arr = self.arr;
        
        [self.dataSource reloadData:nil];
        
        if([self.tableDelegate respondsToSelector:@selector(didRefresh)]){
            
            [self.tableDelegate didRefresh];
            
        }
        
    }else{//using model & server
        
        if(self.isLoading){
            
            return;
            
        }
        
        self.isLoading = YES;
        
        if(self.needsServerRefresh){
            
            [self loadDataFromServer];
            
        }else{
            
            [self loadDataFromLocalDataStore];
            
        }
        
        self.isLoading = NO;
        
        if([self.tableDelegate respondsToSelector:@selector(didRefresh)]){
            
            [self.tableDelegate didRefresh];
            
        }
        
    }
    
}

- (void)loadDataFromLocalDataStore{
    
    NSArray *unsortedArray = [[self entityClass] MR_findAllWithPredicate:self.predicate];
    
    NSArray *anArray = nil;
    
    if(self.arrayOfSortDescriptors.count == 0){
        
        anArray = unsortedArray;
        
    }else{
        
        anArray = [unsortedArray sortedArrayUsingDescriptors:self.arrayOfSortDescriptors];
        
    }
    
    self.dataSource.arr = anArray;
    
    [self.dataSource reloadData:nil];
    
}

- (void)loadDataFromServer{
    
    NSParameterAssert(self.serverParameters);
    
    @weakify(self);
    
    HDKBaseSyncService *syncService = [HDKBaseSyncService new];
    
    [syncService getObjects:[self entityClass] fromServerWithDict:self.serverParameters path:self.requestPath withRelated:self.related andCompletion:^{
        
        @strongify(self);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            @strongify(self);
            
            [self loadDataFromLocalDataStore];
            
        });
        
    }andError:nil];
    
}

- (void)moveToTableTopAfterDelay:(NSTimeInterval)seconds{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height) animated:YES];
        
    });
    
}

- (void)moveToTableBottomAfterDelay:(NSTimeInterval)seconds{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self.tableView scrollRectToVisible:CGRectMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height, self.tableView.bounds.size.width, self.tableView.bounds.size.height) animated:YES];
        
    });
    
}

#pragma mark - HDKDataSourceable
- (void)didSelectRowWithObject:(id)anObject andCompletion:(HDKCompletionBlock)completion{
    
    assert(@"subclass only");
    
}

#pragma mark - HDKBaseSyncServiceDelegate
- (void)syncServiceDidComplete{
    
    NSLog(@"- sync completed");
    
}


@end
