//
//  HDKTableBackedViewController.h
//  HarborKit
//
//  Created by Joseph Gorecki on 8/13/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

@import UIKit;
@import CoreData;

#import "HDKBaseViewController.h"
#import "HDKBaseDataSource.h"
#import "HDKDataSourceConstants.h"

@protocol HDKTableBackedViewControllerDelegate <NSObject>

- (void)didRefresh;

@end

@interface HDKTableBackedViewController : HDKBaseViewController

@property (strong, nonatomic) Class entityClass;
@property (copy, nonatomic) NSString *requestPath;

@property (strong, nonatomic) HDKBaseDataSource *dataSource;

@property (strong, nonatomic) NSDictionary *serverParameters;
@property (strong, nonatomic) NSArray *arrayOfSortDescriptors;
@property (strong, nonatomic) NSPredicate *predicate;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (readwrite) NSInteger offset;
@property (readwrite) NSInteger limit;

@property (strong, nonatomic) id<HDKTableBackedViewControllerDelegate>tableDelegate;

@property (strong, nonatomic) id related;

@property (readwrite) BOOL needsServerRefresh;
@property (readwrite) BOOL editable;

@property (strong, nonatomic) NSArray *arr;

- (void)configureAndCreateTableWithDataSource:(Class)aDataSource andArray:(NSArray *)anArray registeredCellClass:(Class)aCellViewClass cellBlock:(HDKCellBlock)cellBlock;
- (void)configureWithDataSource:(Class)aDataSource andArray:(NSArray *)anArray registeredCellClass:(Class)aCellViewClass cellBlock:(HDKCellBlock)cellBlock;
- (void)configureWithDataSource:(Class)aDataSource andArray:(NSArray *)anArray registeredCellClass:(Class)aCellViewClass registeredHeaderClass:(Class)aHeaderViewClass cellBlock:(HDKCellBlock)cellBlock;

- (void)refresh;

- (void)moveToTableBottomAfterDelay:(NSTimeInterval)seconds;

- (void)moveToTableTopAfterDelay:(NSTimeInterval)seconds;

@end
