//
//  HDKGridBackedViewController.h
//  HarborKit
//
//  Created by Joseph Gorecki on 8/24/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKTableBackedViewController.h"
#import "HDKBaseGridDataSource.h"
#import "HDKCollectionViewCell.h"

@interface HDKGridBackedViewController : HDKTableBackedViewController

@property (strong, nonatomic) IBOutlet UICollectionView *gridView;

- (void)configureWithDataSource:(Class)aDataSource andArray:(NSArray *)anArray registeredCellClass:(Class)aCellViewClass registeredHeaderClass:(Class)aHeaderViewClass cellBlock:(HDKGridCellBlock)cellBlock;
- (void)configureWithDataSource:(Class)aDataSource andArray:(NSArray *)anArray registeredCellClass:(Class)aCellViewClass cellBlock:(HDKGridCellBlock)cellBlock;

@end
