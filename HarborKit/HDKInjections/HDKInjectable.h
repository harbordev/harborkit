//
//  HDKInjectable.h
//  HarborDevKit
//
//  Created by Joseph Gorecki on 3/3/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppleGuice/AppleGuice.h>

@protocol HDKInjectable <NSObject, AppleGuiceInjectable>

@required

- (void)startInjectingService;

@end