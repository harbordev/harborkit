//
//  HDKInjectionLoader.h
//  Favement
//
//  Created by Joseph Gorecki on 3/3/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HDKInjectionLoader : NSObject

- (void)loadInjections;

@end
