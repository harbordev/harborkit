//
//  HDKInjectionLoader.m
//  Favement
//
//  Created by Joseph Gorecki on 3/3/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKInjectionLoader.h"
#import "HDKInjectable.h"

@interface HDKInjectionLoader ()

@property (nonatomic, strong) NSArray *ioc_HDKInjectable;

@end

@implementation HDKInjectionLoader

- (void)loadInjections{

    for (id <HDKInjectable> injection in self.ioc_HDKInjectable) {
    
        [injection startInjectingService];
    
    }

}

@end
