//
//  HDKTextView.m
//  HarborKit
//
//  Created by Joseph Gorecki on 9/9/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKTextView.h"

@interface HDKTextView ()

@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, strong) UIFont *placeholderFont;
@property (nonatomic, strong) UIColor *placeholderColor;

@end

@implementation HDKTextView

- (instancetype)initWithFrame:(CGRect)frame {
    
    if ((self = [super initWithFrame:frame])) {
        
        [self awakeFromNib];
        
    }
    
    return self;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChange:) name:UITextViewTextDidChangeNotification object:self];
    
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)setPlaceholder:(NSString *)aPlaceholder withFont:(UIFont *)aFont withPlaceHolderColor:(UIColor *)aColor{

    self.placeholderColor = aColor;
    self.placeholderFont = aFont;
    
    self.placeholder = aPlaceholder;
    
}

- (void)setPlaceholder:(NSString *)aPlaceholder {
    
    if (aPlaceholder != _placeholder) {
        
        _placeholder = aPlaceholder;
        
    }
    
    _placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(7, 3.5, self.frame.size.width, 30)];

    _placeholderLabel.textColor = _placeholderColor;
    _placeholderLabel.text = _placeholder;
    _placeholderLabel.font = _placeholderFont;
    
    _placeholderLabel.tag = 1;
    
    [self addSubview:_placeholderLabel];
    
}

- (void)setText:(NSString *)text{
    
    [super setText:text];
    
    [self didChange:nil];
    
}

- (void)didChange:(NSNotification *)notification {
    
    if (self.text.length == 0 || self.text == nil) {
        
        [self addSubview:self.placeholderLabel];
        
    }else{
        
        [[self viewWithTag:1] removeFromSuperview];
    }
    
}

@end
