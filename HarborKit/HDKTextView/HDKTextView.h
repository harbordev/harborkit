//
//  HDKTextView.h
//  HarborKit
//
//  Created by Joseph Gorecki on 9/9/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HDKTextView : UITextView

/**
 * Setup HDKTextview with placeholder information
 */
- (void)setPlaceholder:(NSString *)aPlaceholder withFont:(UIFont *)aFont withPlaceHolderColor:(UIColor *)aColor;

/**
 * Exposed label for placeholder label in HDKTextView
 */
@property (nonatomic, strong) UILabel *placeholderLabel;

@end
