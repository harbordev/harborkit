//
//  HDKUserObject.m
//  HarborKit
//
//  Created by Joseph Gorecki on 7/13/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import "HDKUserObject.h"
//#import <TTTURLRequestFormatter.h>
#import <OMGHTTPURLRQ/OMGHTTPURLRQ.h>
#import "NSMutableURLRequest+HarborKit.h"
#import <AutoCoding/AutoCoding.h>

#import "HDKHudHelper.h"
#import "HDKHttpResponseHelper.h"

NSString *HDKUserDefaults = @"HDKUserDefaultsStorageObject";
NSString *HDKUserDefaultsWasChanged = @"HDKUserDefaultsWasChanged";
NSString *HDKUserDefaultsWasAdded = @"HDKUserDefaultsWasAdded";

@implementation HDKUserObject

#define HDK_USEROBJECT_NULL_PROTECT(value) ( ((NSNull*)value == [NSNull null]) ? (nil) : (value) )

#pragma mark -
- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:_email forKey:@"email"];
    [aCoder encodeObject:_userId forKey:@"userId"];
    [aCoder encodeObject:_key forKey:@"key"];
    [aCoder encodeObject:_username forKey:@"username"];
    [aCoder encodeObject:_role forKey:@"role"];
    [aCoder encodeObject:_territoryId forKey:@"territoryId"];
    [aCoder encodeObject:_notications forKey:@"notications"];
    [aCoder encodeObject:_fbuserId forKey:@"fbuserId"];
    [aCoder encodeObject:_avatarURLString forKey:@"avatarURLString"];
    [aCoder encodeObject:_fullname forKey:@"fullname"];
    [aCoder encodeObject:_lastname forKey:@"lastname"];
    [aCoder encodeObject:_firstname forKey:@"firstname"];
    [aCoder encodeObject:_gender forKey:@"gender"];
    [aCoder encodeObject:_dob forKey:@"dob"];
    [aCoder encodeObject:_bio forKey:@"bio"];
    [aCoder encodeObject:_hasChanges forKey:@"hasChanges"];
    [aCoder encodeObject:_inviteCode forKey:@"inviteCode"];
    [aCoder encodeObject:_pinCode forKey:@"pinCode"];
    [aCoder encodeObject:_displayname forKey:@"displayname"];
    [aCoder encodeObject:_phonenumber forKey:@"phonenumber"];
    [aCoder encodeObject:_cellnumber forKey:@"cellnumber"];
    [aCoder encodeObject:_chatcount forKey:@"chatcount"];
    [aCoder encodeObject:_friendcount forKey:@"friendcount"];
    [aCoder encodeObject:_acceptsPushNotications forKey:@"acceptsPushNotications"];
    [aCoder encodeObject:_mobileadmin forKey:@"mobileadmin"];
    [aCoder encodeObject:_metaStorageDictionary forKey:@"metaStorageDictionary"];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    
    if(self = [super initWithCoder:aDecoder]){
        
        _email = [aDecoder decodeObjectForKey:@"email"];
        _userId = [aDecoder decodeObjectForKey:@"userId"];
        _username = [aDecoder decodeObjectForKey:@"username"];
        _key = [aDecoder decodeObjectForKey:@"key"];
        _role = [aDecoder decodeObjectForKey:@"role"];
        _territoryId = [aDecoder decodeObjectForKey:@"territoryId"];
        _notications = [aDecoder decodeObjectForKey:@"notications"];
        _fbuserId = [aDecoder decodeObjectForKey:@"fbuserId"];
        _avatarURLString = [aDecoder decodeObjectForKey:@"avatarURLString"];
        _fullname = [aDecoder decodeObjectForKey:@"fullname"];
        _lastname = [aDecoder decodeObjectForKey:@"lastname"];
        _firstname = [aDecoder decodeObjectForKey:@"firstname"];
        _gender = [aDecoder decodeObjectForKey:@"gender"];
        _dob = [aDecoder decodeObjectForKey:@"dob"];
        _bio = [aDecoder decodeObjectForKey:@"bio"];
        _hasChanges = [aDecoder decodeObjectForKey:@"hasChanges"];
        _inviteCode = [aDecoder decodeObjectForKey:@"inviteCode"];
        _pinCode = [aDecoder decodeObjectForKey:@"pinCode"];
        _displayname = [aDecoder decodeObjectForKey:@"displayname"];
        _cellnumber = [aDecoder decodeObjectForKey:@"cellnumber"];
        _phonenumber = [aDecoder decodeObjectForKey:@"phonenumber"];
        _chatcount = [aDecoder decodeObjectForKey:@"chatcount"];
        _friendcount = [aDecoder decodeObjectForKey:@"friendcount"];
        _acceptsPushNotications = [aDecoder decodeObjectForKey:@"acceptsPushNotications"];
        _mobileadmin = [aDecoder decodeObjectForKey:@"mobileadmin"];
        _metaStorageDictionary = [aDecoder decodeObjectForKey:@"metaStorageDictionary"];
        
    }
    
    return self;
    
}

+ (HDKUserObject *)getUserObjectFromDefaults{
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:HDKUserDefaults]){
        
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:HDKUserDefaults];
        
        HDKUserObject *aUserObject = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        return aUserObject;
        
    }else{
        
        return nil;
        
    }
    
}

+ (void)saveUserObjectToDefault:(HDKUserObject *)aUserObject{
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:aUserObject];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:HDKUserDefaults];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:HDKUserDefaultsWasAdded
                                                        object:nil];
    
}

+ (void)removeUserObjectFromDefaults{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:HDKUserDefaults];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:HDKUserDefaultsWasChanged
                                                        object:nil];
    
}

+ (HDKUserObject *)mapServerDictionaryToObject:(NSDictionary *)aDictionary{
    
    NSParameterAssert([aDictionary isKindOfClass:[NSDictionary class]]);
    
    HDKUserObject *aUserObject = [HDKUserObject getUserObjectFromDefaults];
    
    if(!aUserObject){
        
        aUserObject = [HDKUserObject new];
        
    }
    
    aUserObject.email = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"email"]);
    aUserObject.key = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"key"]);
    aUserObject.userId = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"userID"]);
    aUserObject.username = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"username"]);
    aUserObject.role = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"role"]);
    aUserObject.avatarURLString = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"profile_image"]);
    aUserObject.fbuserId = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"facebook_id"]);
    aUserObject.fullname = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"fullname"]);
    aUserObject.lastname = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"lastname"]);
    aUserObject.firstname = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"firstname"]);
    aUserObject.gender = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"gender"]);
    aUserObject.dob = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"dob"]);
    aUserObject.bio = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"bio"]);
    aUserObject.phonenumber = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"phonenumber"]);
    aUserObject.cellnumber = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"cellnumber"]);
    aUserObject.displayname = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"displayname"]);
    aUserObject.chatcount = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"chatcount"]);
    aUserObject.friendcount = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"friendcount"]);
    aUserObject.pinCode = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"pin_code"]);
    aUserObject.acceptsPushNotications = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"push_notifications_active"]);
    aUserObject.mobileadmin = HDK_USEROBJECT_NULL_PROTECT(aDictionary[@"mobile_admin"]);
    
    return aUserObject;
    
}

+ (void)syncUserObjectWithServer:(NSString *)aPathString withParameters:(NSDictionary *)parameters completion:(HDKUserObjectCompletionBlock)completion error:(HDKUserObjectCompletionBlock)errorBlock{
    
    NSParameterAssert(aPathString.length > 0);
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSMutableURLRequest *request = [OMGHTTPURLRQ POST:aPathString JSON:parameters];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    HDKUserObject *aUserObject = [HDKUserObject getUserObjectFromDefaults];
    
    if(aUserObject.key.length > 0){
        
        [request addValue:[NSString stringWithFormat:@"ApiKey %@:%@", aUserObject.username, aUserObject.key] forHTTPHeaderField:@"Authorization"];
        
    }
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data){
            
            if(errorBlock){
                errorBlock();
            }
            
            [HDKHudHelper dismiss];
            
            return;
            
        }
        
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        if (!error && [HDKHttpResponseHelper isInValidRange:response]) {
            
            [HDKUserObject saveUserObjectToDefault:[HDKUserObject mapServerDictionaryToObject:json]];
            
            if(completion){
                completion();
            }
            
        }else{
            
            ////NSLog(@"error: json: %@ response: %@", response);
            
            if(errorBlock){
                errorBlock();
            }
            
        }
        
    }];
    
    [task resume];
    
}

+ (void)updateUserAvatar:(NSString *)aPathString withParameters:(NSDictionary *)parameters andImage:(UIImage *)anImage completion:(HDKUserObjectCompletionBlock)completion error:(HDKUserObjectCompletionBlock)errorBlock{
    
    NSParameterAssert(aPathString.length > 0);
    NSParameterAssert(anImage);
    
    HDKUserObject *aUserObject = [HDKUserObject getUserObjectFromDefaults];
    
    NSMutableURLRequest *request = [NSMutableURLRequest hdk_requestForMultiform:aPathString withImage:anImage withID:nil withInputName:@"image" withJson:parameters];
    
    NSURLResponse *aResponse = nil;
    NSError *aError = nil;
    
    NSData *aData = [NSURLConnection sendSynchronousRequest:request returningResponse:&aResponse error:&aError];
    
    id json = [NSJSONSerialization JSONObjectWithData:aData options:0 error:&aError];
    
    if (!aError && ([(NSHTTPURLResponse *)aResponse statusCode] == 200)) {
        
        //NSLog(@"swapping out the avatar and setting in user object now.");
        
        aUserObject.avatarURLString = json[@"profile_image"];
        
        [HDKUserObject saveUserObjectToDefault:aUserObject];
        
        if(completion){
            
            completion();
            
        }
        
    }else{
        
        //NSLog(@"error: json: %@ response: %@", json, aResponse);
        
        if (errorBlock){
            
            errorBlock();
            
        }
        
    }
    
}

+ (void)changeNotificationStatus:(HDKUserObjectCompletionBlock)completion{//unused?
    
    HDKUserObject *aUserObject = [HDKUserObject getUserObjectFromDefaults];
    
    if( [aUserObject.notications boolValue] == NO ){
        
        aUserObject.notications = @(YES);
        
    }else{
        
        aUserObject.notications = @(NO);
        
    }
    
    [HDKUserObject saveUserObjectToDefault:aUserObject];
    
}

@end
