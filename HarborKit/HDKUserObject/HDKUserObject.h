//
//  HDKUserObject.h
//  HarborKit
//
//  Created by Joseph Gorecki on 7/13/15.
//  Copyright (c) 2015 HarborDev. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

typedef void(^HDKUserObjectCompletionBlock)(void);

@interface HDKUserObject : NSObject

/**
 * Name of custom user object.
 */
extern NSString * HDKUserDefaults;

/**
 * Notifications for custom user object changes.
 */
extern NSString * HDKUserDefaultsWasChanged;

/**
 * Notifications for when a user object is created.
 */
extern NSString * HDKUserDefaultsWasAdded;

/**
 * User Id on the server.
 */
@property (strong, nonatomic) NSNumber *userId;

/**
 * Facebook User Id on the server as a string.
 */
@property (strong, nonatomic) NSString *fbuserId;

/**
 * User email.  Doubles as username.
 */
@property (copy, nonatomic) NSString *email;

/**
 * User key.  Required for API requests.
 */
@property (copy, nonatomic) NSString *key;

/**
 * User username.  Same as email.
 */
@property (copy, nonatomic) NSString *username;

/**
 * User role which is used for permissions.
 */
@property (copy, nonatomic) NSString *role;

/**
 * User's preferred territory - local.
 */
@property (strong, nonatomic) NSNumber *territoryId;

/**
 * User's boolean for notification reciept
 */
@property (strong, nonatomic) NSNumber *notications;

/** 
 * User's avatar image
 */
@property (copy, nonatomic) NSString *avatarURLString;

/**
* User's DOB as string
*/
@property (copy, nonatomic) NSString *dob;

/**
 * User's Gender as string
 */
@property (copy, nonatomic) NSString *gender;

/**
 * User's First Name
 */
@property (copy, nonatomic) NSString *firstname;

/**
 * User's Last Name
 */
@property (copy, nonatomic) NSString *lastname;

/**
 * User's Full Name
 */
@property (copy, nonatomic) NSString *fullname;

/**
 * User's Biography
 */
@property (copy, nonatomic) NSString *bio;

/** 
 * Set whether the user object has changes
 */ 
@property (strong, nonatomic) NSNumber *hasChanges;

/**
 * Save Invite code to the user object
 */
@property (copy, nonatomic) NSString *inviteCode;

/**
 * Save security pin code to the user object
 */
@property (copy, nonatomic) NSString *pinCode;

/**
 * Save phone number to the user object
 */
@property (copy, nonatomic) NSString *phonenumber;

/**
 * Save cell to the user object
 */
@property (copy, nonatomic) NSString *cellnumber;

/**
 * Save display name to the user object
 */
@property (copy, nonatomic) NSString *displayname;

/**
 * Save chat count to the user object
 */
@property (copy, nonatomic) NSNumber *chatcount;

/**
 * Save friends count to the user object
 */
@property (copy, nonatomic) NSNumber *friendcount;

/**
 * Saves push notifcation acceptance from backenc profile
 */
@property (copy, nonatomic) NSNumber *acceptsPushNotications;

/**
 * Mobile Admin
 */
@property (copy, nonatomic) NSNumber *mobileadmin;

/**
 * Meta Dictionary for storage -- Key values only!
 */
@property (strong, nonatomic) NSDictionary *metaStorageDictionary;

/**
 * Unarchives and returns the custom object stored in userdefaults
 */
+ (HDKUserObject *)getUserObjectFromDefaults;

/**
 * Archives and saves the custom object.  Call this whenever a change is made to a user object
 */
+ (void)saveUserObjectToDefault:(HDKUserObject *)aUserObject;

/**
 * Kills the custom object stored in the user default and posts a notification that it has changed
 */
+ (void)removeUserObjectFromDefaults;

/**
 * Makes an asynchronous post request to the authentication endpoint for userdetails, parses the response, and stores it in userdefaults.
 */
+ (void)syncUserObjectWithServer:(NSString *)aPathString withParameters:(NSDictionary *)parameters completion:(HDKUserObjectCompletionBlock)completion error:(HDKUserObjectCompletionBlock)errorBlock;

/**
 * Update the image avatar synchronously
 */
+ (void)updateUserAvatar:(NSString *)aPathString withParameters:(NSDictionary *)parameters andImage:(UIImage *)anImage completion:(HDKUserObjectCompletionBlock)completion error:(HDKUserObjectCompletionBlock)errorBlock;

/**
 * Changes the notification status for the logged in user
 */
+ (void)changeNotificationStatus:(HDKUserObjectCompletionBlock)completion;

@end
