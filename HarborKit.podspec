Pod::Spec.new do |s|
  s.name         = 'HarborKit'
  s.version      = '1.10.23'
  s.requires_arc = true
  s.ios.deployment_target = '6.1'  
  s.summary      = 'HarborDev Toolkit'
  s.homepage     = 'https://bitbucket.org/harbordev' 
  s.license      = {:type => 'MIT', :file => 'LICENSE'}
  s.author = {
    'Joe Gorecki' => 'j@harbordev.com'
  }
  s.source = {
    :git => 'https://bitbucket.org/harbordev/harborkit.git',
    :tag => '1.10.23'
  }
  s.framework    = 'CoreData'
  s.header_dir   = 'HarborKit'  
  s.source_files = 'HarborKit/**/*.{h,m}'
  s.resources = 'HarborKit/**/*.{xib}'
  s.dependency     'SVProgressHUD'
  s.dependency     'Navajo'
  s.dependency     'AutoCoding'
  s.dependency     'OMGHTTPURLRQ', '~>2.1.3'
  s.dependency     'SVProgressHUD'
  s.dependency     'Bolts'
  s.dependency     'SSPullToRefresh'
  s.dependency     'MagicalRecord', '~> 2.3.0'
  s.dependency     'FormatterKit', '~> 1.7' 
  s.dependency     'PromiseKit','~> 1.4'  
  s.dependency     'ObjectiveSugar', '~> 1.1.0'
  s.dependency     'AppleGuice', '~> 1.3.0'
  s.dependency     'SVWebViewController'
  s.dependency     'TPKeyboardAvoiding'
  s.dependency     'libextobjc'
  s.dependency     'MWFeedParser', '~>1.0'
  s.dependency     'AWSCore', '~>2.4.12'
  s.dependency     'AWSAutoScaling', '~>2.4.12'
  s.dependency     'AWSCloudWatch', '~>2.4.12'
  s.dependency     'AWSDynamoDB', '~>2.4.12'
  s.dependency     'AWSEC2', '~>2.4.12'
  s.dependency     'AWSElasticLoadBalancing', '~>2.4.12'
  s.dependency     'AWSKinesis', '~>2.4.12'
  s.dependency     'AWSLambda', '~>2.4.12'
  s.dependency     'AWSMachineLearning', '~>2.4.12'
  s.dependency     'AWSMobileAnalytics', '~>2.4.12'
  s.dependency     'AWSS3', '~>2.4.12'
  s.dependency     'AWSSES', '~>2.4.12'
  s.dependency     'AWSSimpleDB', '~>2.4.12'
  s.dependency     'AWSSNS', '~>2.4.12'
  s.dependency     'AWSSQS', '~>2.4.12'
  s.dependency     'AWSCognito', '~>2.4.12'
  s.dependency     'FBSDKCoreKit', '~> 4.14.0' 
  s.dependency     'FBSDKShareKit', '~> 4.14.0'
  s.dependency     'FBSDKLoginKit', '~> 4.14.0'
  s.dependency     'CWStatusBarNotification'
  s.dependency     'DZNEmptyDataSet', '~> 1.7.3'
  s.dependency     'GPUImage'
  s.dependency     'DateTools'
  s.dependency     'SDWebImage', '~> 4.0.0'

  s.prefix_header_contents = <<-EOS
#import <CoreData/CoreData.h>
EOS

end